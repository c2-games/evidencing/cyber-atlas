import { sveltekit } from '@sveltejs/kit/vite';
// import houdini from 'houdini/vite';
import { defineConfig } from 'vite';
import precomputedNodesPlugin from './src/vite-plugins/vite-plugin-svelte-virtual-precomputed-nodes.js';

export default defineConfig({
  // plugins: [houdini(), sveltekit()]
  plugins: [precomputedNodesPlugin(), sveltekit()]
});

# CAE Cyber Competitions Atlas

A resource to help connect people with competitions in the cyber security space.
Driven by data provided by surveys of the CAE community.

## Developing

Install dependencies with `npm install`

To start the app in development mode, run `npm run dev`

## User Tracking

This project can optionally use Umami for user tracking. The following custom events are tracked:

Last update: 8/21/2024

- Proxi Play (map):
  - Node Select (with newly selected node name)
  - Node deselect (with previously selected node name)
  - Show / Hide / Never show help dialog
  - Close / Collapse Sidebar
- Hide Footer Warning
- Toggle UI theme (light/dark mode)
- View Pathway (with pathway name)
- Competition List Card (competition list / pathways / promoted comps)
  - View dropdown of links (with competition name)
  - External Link clicked (from dropdown or standalone link) (with competition name and link name/href/description/category)
  - View competition on Map (with competition name)
  - View competition Details (with competition name)
- Share button is pressed to generate a link

### UTM Tracking

Share links utilize a `utm_source` and `utm_content`. `utm_source=share_link` means the link was generated with the
share button, and `content=proxiplay` means it came off the map sidebar rather than the competition page.

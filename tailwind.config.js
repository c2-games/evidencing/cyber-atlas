/** @type {import('tailwindcss').Config} */
export default {
  content: ['./src/**/*.{html,js,svelte,ts}', './node_modules/flowbite-svelte/**/*.{html,js,svelte,ts}'],
  theme: {
    extend: {
      backgroundImage: {
        main: "url('/main_background.png')",
        binary: "url('/binary_bg.png')"
      },
      colors: {
        'cyber-blue': {
          500: '#368DE3',
          600: '#106097',
          800: '#193d66',
          900: '#0C2856'
        },
        'cyber-gray': {
          100: '#FEFEFE',
          200: '#F2F2F2',
          300: '#D5D5D5',
          500: '#BFC7CB',
          600: '#74879C',
          800: '#333333'
        },
        secondary: '#E3574D',
        // flowbite-svelte default color
        primary: {
          // flowbite teal
          50: '#EDFAFA',
          100: '#D5F5F6',
          200: '#AFECEF',
          300: '#7EDCE2',
          400: '#16BDCA',
          500: '#0694A2',
          600: '#047481',
          700: '#036672',
          800: '#05505C',
          900: '#014451'
        },
        // sveltvet dark background color
        'medium-extra-grey': '#454545',
        'medium-extra-gray': '#454545',
        // sveltvet outline color
        'lite-extra-grey': '#6b6b6b',
        'lite-extra-gray': '#6b6b6b'
      },
      fontSize: {
        xxl: ['16rem', '1']
      },
      fontFamily: {
        poppins: ['Poppins', 'sans-serif'],
        'nunito-sans': ['"Nunito Sans"']
      }
    }
  },
  safelist: [
    'hidden',
    {
      // width/height/padding/margin classes because some are used dynamically (ex, <Spinner.svelte>)
      pattern: /(w|h|p[se]?|m[se]?)-([0-9]+)/
    }
  ],
  plugins: [require('flowbite/plugin')],
  darkMode: 'class'
};

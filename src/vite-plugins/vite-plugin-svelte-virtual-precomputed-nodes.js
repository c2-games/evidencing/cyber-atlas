import EngagementData from '../lib/data/rawData';
import ELK from 'elkjs/lib/elk.bundled.js';

// returns: Promise<Map<string, SvelvetNodeInfo>>
async function LayoutNodes(
  nodes, // Array<GraphNode>
  edges, // Array<GraphEdge>
  default_width = 200,
  default_height = 100,
  strategy = 'org.eclipse.elk.force'
) {
  // This will be the nodes getting returned from the function
  const ret_nodes = new Map(); // Map<string, SvelvetNodeInfo>()
  // This will be the node info for layout
  const _elk_nodes = []; // Array<ElkNode>()
  nodes.forEach((node) => {
    ret_nodes.set(node.id, {
      id: node.id,
      width: node.width || default_width,
      height: node.height || default_height,
      data: node.data,
      // Add an empty connections array for the future edges
      connections: [],
      // This will get overwritten later
      position: { x: 0, y: 0 }
    });
    _elk_nodes.push({
      id: node.id,
      // Need some dimensions so the layout engine knows the node bounds
      width: node.width || default_width,
      height: node.height || default_height
    });
  });
  const ret_edges = new Map(); // Map<string, SvelteConnectionInfo>()
  const _elk_edges = edges.map((edge) => {
    const source = edge['source'];
    const target = edge['target'];
    ret_edges.set(edge.id, {
      id: edge.id,
      target: target,
      data: edge.data
    });
    const _edge = {
      id: edge['id'],
      sources: [source],
      targets: [target],
      source: source,
      target: target,
      arrow: true
    };
    return _edge;
  });

  const graph = {
    id: 'lineage',
    layoutOptions: {
      'elk.algorithm': strategy,
      'org.eclipse.elk.direction': 'UNDEFINED',
      'org.eclipse.elk.spacing.nodeNode': 10
    },
    children: _elk_nodes,
    edges: _elk_edges
  };

  const elk = new ELK();
  const layout = await elk.layout(graph);

  // Loop over the nodes being laid out, pull out the position data
  if (layout?.children) {
    for (const node of layout.children) {
      const n = ret_nodes.get(node.id);
      if (n === undefined) {
        continue;
      }
      n.position = { x: node.x || 0, y: node.y || 0 };
    }
  }

  // Loop over all the edges and add the connection targets to the source nodes

  for (const e of layout.edges || []) {
    for (const s of e.sources) {
      const n = ret_nodes.get(s);
      if (n === undefined) {
        continue;
      }
      for (const t of e.targets) {
        n.connections.push({ id: e.id, target: t, data: ret_edges.get(e.id)?.data || {} });
      }
    }
  }
  return ret_nodes;
}

function slugify(str) {
  return str
    .toLowerCase()
    .trim()
    .replace(/[^\w\s-]/g, '')
    .replace(/[\s_-]+/g, '-')
    .replace(/^-+|-+$/g, '');
}

function makeEventNodeId(name) {
  return `event-${slugify(name)}`;
}

function makeEventEventEdgeId(event1_name, event2_name) {
  return `e-${slugify(event1_name)}-${slugify(event2_name)}`;
}

async function initialize() {
  let events = Object.values(EngagementData.data.competitions);
  let nodeInfo = null;
  let nodes = new Map();
  let edges = new Map();
  let eventsByTag = new Map();
  let max_participation = 1;
  // Calculate max participation
  for (let participants of Object.values(EngagementData.data.participation)) {
    max_participation = Math.max(max_participation, participants);
  }
  // Build a hashmap of competitions by tag
  for (let event of events) {
    const eventTags = event.properties.tags || [];
    for (const tag of eventTags) {
      let eventList = eventsByTag.get(tag) || [];
      console.log(`${event.name} eventList for ${tag}: ${eventList}`);
      eventList.push(event.name);
      console.log(eventList);
      eventsByTag.set(tag, eventList);
    }
  }
  // console.log(eventsByTag)
  for (let event of events) {
    const width_mod = 100;
    let participants = event.properties.participation;
    nodes.set(makeEventNodeId(event.name), {
      id: makeEventNodeId(event.name),
      width: (participants / max_participation) * (width_mod * 2) + width_mod,
      height: (participants / max_participation) * (width_mod * 2) + width_mod,
      data: {
        name: event.name,
        type: 'Event',
        tags: event.properties.tags || []
      }
    });
    // skip events without participation
    if (!participants) {
      continue;
    }
    // Higher participation nodes should be connected to more nodes to be centered
    const gravity = participants / max_participation;
    if (gravity < 0.1) {
      continue;
    }
    for (let ev_name of Object.keys(EngagementData.data.participation)) {
      // skip training nodes
      if (Object.keys(EngagementData.data.training).includes(ev_name)) {
        continue;
      }
      // Check if the reverse direction already exists
      // TODO: Move this to the layout function? We only care about
      //   duplicates because it messes up the layout algorithm
      if (edges.get(makeEventEventEdgeId(ev_name, event.name)) !== undefined) {
        continue;
      }
      edges.set(makeEventEventEdgeId(event.name, ev_name), {
        id: makeEventEventEdgeId(event.name, ev_name),
        source: makeEventNodeId(event.name),
        target: makeEventNodeId(ev_name),
        data: {
          type: 'PARTICIPATION_GROUPING',
          gravity: participants / max_participation
        }
      });
    }
  }
  console.log('laying out nodes');
  let ret = await LayoutNodes([...nodes.values()], [...edges.values()], 100, 100);
  console.log('layout complete');
  console.log('adding edges by tag');
  // enhance nodeInfo with edges that aren't used for layout
  for (const event of events) {
    const eventTags = event.properties.tags || [];
    let eventNodeInfo = ret.get(makeEventNodeId(event.name));
    for (const tag of eventTags) {
      const eventList = eventsByTag.get(tag) || [];
      for (const name of eventList) {
        if (name == event.name) {
          continue;
        }
        const edgeId = makeEventEventEdgeId(event.name, name) + '-tag';
        let edge = eventNodeInfo.connections.find((e) => e.id == edgeId);
        if (!edge) {
          edge = {
            id: edgeId,
            target: makeEventNodeId(name),
            data: {
              type: 'TAG_SHARED',
              tags: []
            }
          };
          eventNodeInfo.connections.push(edge);
        }
        edge.data.tags.push(tag);
      }
      // reduce the connections to just the top few
      eventNodeInfo.connections = [...getTopConnections(eventNodeInfo.connections)];
      // console.log(`updated ${event.name} with ${tag} events`)
      ret.set(makeEventNodeId(event.name), eventNodeInfo);
      // console.log(eventNodeInfo)
    }
  }
  console.log('edges by tag added');
  nodeInfo = [...ret.values()];
  return nodeInfo;
}

function getTopConnections(connections) {
  return connections
    .filter((c) => {
      return c.data.type == 'TAG_SHARED';
    })
    .sort((a, b) => {
      a.data.tags.length - b.data.tags.length;
    })
    .slice(0, 25);
}

export default function precomputedNodesPlugin() {
  const virtualModuleId = 'virtual:precomputed-nodes';
  const resolvedVirtualModuleId = '\0' + virtualModuleId;

  return {
    name: 'precomputed-nodes', // required, will show up in warnings and errors
    resolveId(id) {
      if (id === virtualModuleId) {
        return resolvedVirtualModuleId;
      }
    },
    async load(id) {
      if (id === resolvedVirtualModuleId) {
        return `export const nodeInfo = ${JSON.stringify(await initialize())}`;
      }
    }
  };
}

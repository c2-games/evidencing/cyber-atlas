export default {
  data: {
    competitions: {
      'A-ISAC Student Cyber Challenge': {
        aliases: ['A-ISAC Student Cyber Challenge', 'Aviation I-SAC Collegiate CTF Competition', 'aviation isac'],
        name: 'A-ISAC Student Cyber Challenge',
        properties: {
          description:
            "Registration for the 2024 Student Cyber Challenge is now open! Any questions about the event? Please reach out to Matt Crandell at mcrandell@a-isac.com. Last Year's Cyber Challenge Sponsors . About. Meet Aviation ISAC CISO Survey Results. Membership. Membership Benefits Community Meetings Events.",
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.a-isac.com/cyberchallenge',
              title: 'Home Page'
            }
          ],
          participation: 4,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      'ADMI Cybersecurity CTF': {
        aliases: ['ADMI Cybersecurity Competition'],
        name: 'ADMI Cybersecurity CTF',
        properties: {
          description:
            'The Association of Computer Science Departments at Minority Institutions (ADMI) will host a Cybersecurity CTF Challenge Competition as part of the 20232 ADMI Symposium on Computing at Minority Institutions. Teams from a number HBCUs will compete for prizes and bragging rights on Friday, April 14, 2023. Each member of the school team(s) must ...',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.admiusa.org/admi2024/cybersecurity.php',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF'],
          teams: null,
          virtual: null
        }
      },
      'AFCEA TechNet Augusta International Military and Collegiate CTF': {
        aliases: ['AFCEA TechNet Augusta International Military and Collegiate Capture The Flag'],
        name: 'AFCEA TechNet Augusta International Military and Collegiate CTF',
        properties: {
          description:
            'TechNet Augusta 2024 gives participants the opportunity to examine and explore the intricacies of the cyber domain. With assistance from the U.S. Army Cyber Center of Excellence and industry experts, the conference is designed to open the lines of communication and facilitate networking, education and problem solving. Leaders and operators also discuss procurement challenges the military, government and industry face during a time of uncertain budgets and runaway technology advances.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://events.afcea.org/Augusta24/Public/enter.aspx',
              title: '2024 Event'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF', 'Conference'],
          teams: null,
          virtual: null
        }
      },
      'Alamo ACE': {
        aliases: ['Alamo ACE'],
        name: 'Alamo ACE',
        properties: {
          description:
            'Alamo ACE is an annual event that provides access and insight to the military community on cyberspace and multi-domain operations. The 2023 event was held at the La Cantera Resort in November 2023 and featured expert panels, keynote speakers, and a golf tournament.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.alamoace.org/',
              title: 'Home Page'
            }
          ],
          participation: 2,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      'Aviation Cyber Initiative Cyber Rodeo CTF': {
        aliases: ['Aviation Cyber Initiative Cyber Rodeo CTF'],
        name: 'Aviation Cyber Initiative Cyber Rodeo CTF',
        properties: {
          description:
            'Terminus airport is in chaos again due to cyberattacks! Airline kiosks, backend servers, terminal flight information displays, transportation security, runway lights, aircraft, and other critical systems at this tier-1 airport have been compromised by some hackers. There are indicators some airport insiders may have colluded with the hackers!',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://erauprescott.cyberskyline.com/events/aci-ctf',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF'],
          teams: null,
          virtual: null
        }
      },
      'BE Smart Hackathon': {
        aliases: ['BE (Black Enterprise) Smart Hackathon'],
        name: 'BE Smart Hackathon',
        properties: {
          description:
            'A group of 180 students have been selected to meet at the American Airline headquarters in Dallas-Fort Worth, Texas, from Thursday, Nov. 2 to Sunday, Nov. 5. Along the way, the BE Smart Hackathon ...',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.blackenterprise.com/be-hackathon-returns/',
              title: 'Home Page'
            }
          ],
          participation: 2,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      'BlueHens CTF': {
        aliases: ['University of Delaware CTF', 'BlueHens CTF', 'bluehens capture-the-flag'],
        name: 'BlueHens CTF',
        properties: {
          description:
            "BlueHens Capture the Flag. This is a past event. University of Delaware's third annual Capture the Flag (CTF) event will take place from Oct 27-29 virtually. CTF allows competitors from across the globe to put their skills to the test as they detect and deflect cyberattacks. Anyone with an interest in cybersecurity is invited to participate.",
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://events.udel.edu/event/bluehens_capture_the_flag',
              title: 'Home Page'
            }
          ],
          participation: 2,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF'],
          teams: null,
          virtual: null
        }
      },
      BuckeyeCTF: {
        aliases: ['BuckeyeCTF', 'buckeye-ctf'],
        name: 'BuckeyeCTF',
        properties: {
          description:
            'Conduct offensive security research and development on embedded systems and applications. Architect, design, develop, debug, and maintain software for embedded devices and systems. BuckeyeCTF is jeopardy-style CTF hosted by the Cybersecurity Club at The Ohio State University. CTF (Capture the Flag) is a hacking competition in which teams ...',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://ctf.osucyber.club/',
              title: 'Home Page'
            }
          ],
          participation: 3,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF'],
          teams: null,
          virtual: null
        }
      },
      'CNY Hackathon': {
        aliases: ['CNY Hackathon', 'central new york hackathon'],
        name: 'CNY Hackathon',
        properties: {
          description:
            'The CNY Hackathon is a regional intercollegiate offensive/defensive cybersecurity competition held at the end of each Fall semester designed to improve the state of computer science and cybersecurity education, and more closely align academia with Central New York industry partners. Students participate in challenges that task each team with ...',
          education_target: ['College'],
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.cnyhackathon.org/',
              title: 'Home Page'
            }
          ],
          participation: 3,
          practice_available: false,
          qualifiers: false,
          tags: ['Red vs Blue', 'Defensive'],
          teams: 'assigned',
          virtual: false
        }
      },
      CSAW: {
        aliases: ['CSAW'],
        name: 'CSAW',
        properties: {
          description:
            'CSAW CTF is an entry-level, jeopardy style CTF, designed for university-level students who are interested in the field of security. Challenges are meant to exhibit fundamental concepts and help students develop their skills. CSAW CTF occurs over two rounds: a Qualifying Round in September and a Final Round in November.',
          education_target: ['Early College', 'College', 'Professional'],
          eligibility: 'Open',
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.csaw.io/ctf',
              title: 'Home Page'
            },
            {
              category: 'website',
              description: null,
              link: 'https://docs.google.com/document/d/e/2PACX-1vQd86UHImwnQxZoLFi8ShZ1DxP37dDegPyjWe40j9mIXlTdp5syPP5DAX0pGJXdpFxCVzklmVQ_wmld/pub',
              title: '2023 Eligibility and Rules'
            }
          ],
          participation: 12,
          practice_available: null,
          qualifiers: true,
          tags: ['CTF', 'Exploitation', 'Reversing', 'Crypto', 'Forensics'],
          teams: null,
          virtual: null
        }
      },
      CUhackit: {
        aliases: ['CH Hackit'],
        name: 'CUhackit',
        properties: {
          description:
            "Spend 24 hours coding and collaborating in the beautiful Watt Family Innovation Center at Clemson University while we handle the food, coffee, and snacks. No matter your major or level of experience, we want to see you create your next hack with us at CUhackit! We're an official MLH Hackathon Event! This code of conduct ensures that all hackers ...",
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://cuhack.it/',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      'Collegiate Cyber Defense Competition': {
        aliases: [
          'Collegiate Cyber Defense Competition (CCDC)',
          'ALCCDC',
          'CCDC',
          'Collegiate Cyber Defense Competition',
          'MACCDC',
          'Mid Atlantic CCDC',
          'Mid-Atlantic Collegiate Cyber Defense Competition',
          'MWCCDC',
          'National CCDC',
          'NECCDC',
          'PRCCDC',
          'RM CCDC',
          'RMCCDC',
          'Rocky Mountain Collegiate Cyber Defense Competition',
          'SE CCDC',
          'SECCDC',
          'Southeast Collegiate Cyber Defense Competition',
          'SWCCDC',
          'Western Regional CCDC',
          'Western Regional Collegiate Cyber Defense Competition',
          'WRCCDC'
        ],
        name: 'Collegiate Cyber Defense Competition',
        properties: {
          description:
            'CCDC competitions ask student teams to assume administrative and protective duties for an existing "commercial" network - typically a small company with 50+ users, 7 to 10 servers, and common Internet services such as a web server, mail server, and e-commerce site.',
          education_target: ['College'],
          eligibility: 'Open',
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.nationalccdc.org/',
              title: 'Home Page'
            },
            {
              category: 'registration',
              description: 'Register for the event',
              link: 'https://www.nationalccdc.org/index.php/competition/competitors/registration',
              title: 'NCCDC Registration'
            },
            {
              category: 'website',
              description: 'View previous participants',
              link: 'https://www.nationalccdc.org/index.php/competition/about-ccdc/past-winners',
              title: 'NCCDC Past Winners'
            },
            {
              category: 'website',
              description: 'Find your CCDC Region',
              link: 'https://www.nationalccdc.org/index.php/competition/regionals',
              title: 'CCDC Regions'
            },
            {
              category: 'website',
              description: null,
              link: 'http://alccdc.org/',
              title: 'At Large Regional: ALCCDC'
            },
            {
              category: 'website',
              description: null,
              link: 'http://www.maccdc.org/',
              title: 'Mid-Atlantic Regional: MACCDC'
            },
            {
              category: 'website',
              description: null,
              link: 'http://www.cssia.org/mwccdc',
              title: 'Mid-West Regional: MWCCDC'
            },
            {
              category: 'website',
              description: null,
              link: 'https://neccdl.org/neccdc/',
              title: 'Northeast Regional: NECCDC'
            },
            {
              category: 'website',
              description: null,
              link: 'http://www.prccdc.com/',
              title: 'Pacific Rim Regional: PRCCDC'
            },
            {
              category: 'website',
              description: null,
              link: 'http://rmccdc.regis.edu/',
              title: 'Rocky Mountain Regional: RMCCDC'
            },
            {
              category: 'website',
              description: null,
              link: 'http://www.seccdc.org/',
              title: 'Southeast Regional: SECCDC'
            },
            {
              category: 'website',
              description: null,
              link: 'http://southwestccdc.com/',
              title: 'Southwest Regional: SWCCDC'
            },
            {
              category: 'website',
              description: null,
              link: 'http://www.wrccdc.org/',
              title: 'Western Regional: WRCCDC'
            }
          ],
          participation: 140,
          practice_available: null,
          qualifiers: true,
          tags: ['Red vs Blue', 'Defensive'],
          teams: 'yes',
          virtual: false
        }
      },
      'Colorado Cyber Games': {
        aliases: ['Colorado Cyber Games'],
        name: 'Colorado Cyber Games',
        properties: {
          description:
            'The Colorado Cyber Resource Center (CCRC), a program of the National Cybersecurity Center (NCC), was excited to announce the launch of the Colorado Cyber Games. This new, unique competition is designed to help employers find vetted talent and help job candidates get connected with those employers. Other cyber competitions, or "capture the flag" style events [\u2026]',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://colorado-crc.com/cyber-games/',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      'Commonwealth Cyber Fusion': {
        aliases: [
          'VA State sponsored Cyber competition at Virginia Military Institute',
          'Commonwealth Cyber Fusion',
          'CyberFusion',
          'Cyber Fusion'
        ],
        name: 'Commonwealth Cyber Fusion',
        properties: {
          description:
            'Virginia Military Institute will co-host the 7th annual Commonwealth Cyber Fusion event with Honorary Chair Senator Mark R. Warner, the Commonwealth Cyber Initiative, and the Virginia Cyber Range onFebruary 23-24, 2024.This is an invitation-only event for the 27 Virginia colleges that are NSA/NHS designated National Centers of Academic Excellence in Cyber Defense.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.virginiacyberrange.org/events/Cyberfusion2024',
              title: 'Home Page'
            }
          ],
          participation: 14,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      'Conquer the Hill: Adventure Edition': {
        aliases: [
          'DOE Conquer the Hill: Adventure Edition',
          'cyberforce adventure edition',
          'DOE Capture the Hill: Adventurer Edition',
          'CyberForce Adventurer Edition'
        ],
        name: 'Conquer the Hill: Adventure Edition',
        properties: {
          description:
            'Conquer the Hill Adventure Edition is a virtual, individual mission that will test participants cyber knowledge, skills, and abilities using anomalies or challenges on a game style board. Participants will be able to traverse the game board categories of NIST NICE and Energy as they see fit and earn points for answering the challenges correctly. The participant with the most points at the end of the competition is the Conqueror of the Hill.',
          education_target: null,
          eligibility: 'US Institutions',
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://cyberforce.energy.gov/conquer-the-hill/adventure-edition/',
              title: 'Home Page'
            },
            {
              category: 'website',
              description: null,
              link: 'https://cyberforce.energy.gov/conquer-the-hill/eligibility/',
              title: 'Eligibility'
            }
          ],
          participation: 2,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      'Conquer the Hill: Reign Edition': {
        aliases: ['DOE Conquer the Hill', 'DOE Capture the Hill: Reign Challenge', 'cyberforce reign edition'],
        name: 'Conquer the Hill: Reign Edition',
        properties: {
          description:
            "Reign Edition is a cybersecurity competition like no other, as this year's competition transpires within a 3D dungeon crawler platformer game. You will make your way through vigorous challenges, interact with Wizards, Dragons and other NPCs, decrypt secret messages, and navigate your way throughout a kingdom under fire and into the depths of a lair to fulfill your quest. However, not everything is as it seems, and you must use your wits and cunning to progress through levels of increasing difficulty to ultimately unravel the truth. Participants should expect to be tested on their knowledge of cybersecurity, computer science, mathematics, cryptography, and critical thinking skills in general. Conquer the Hill\u2122: Reign Edition 2024 is set for July 13, 2024. Registration opens on June 3 at 8am CT and closes on June 28 at 5pm CT.",
          education_target: null,
          eligibility: 'US Institutions',
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://cyberforce.energy.gov/conquer-the-hill/reign-edition/',
              title: 'Home Page'
            },
            {
              category: 'website',
              description: 'Registration opens on June 3 at 8am CT and closes on June 28 at 5pm CT.',
              link: 'https://forms.office.com/g/eiH1NAA3SE',
              title: '2024 registration'
            },
            {
              category: 'website',
              description: null,
              link: 'https://cyberforce.energy.gov/conquer-the-hill/eligibility/',
              title: 'Eligibility'
            }
          ],
          participation: 4,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      'Cyber 9/12': {
        aliases: ['Cyber 9/12', 'cyber 9-12'],
        name: 'Cyber 9/12',
        properties: {
          description:
            'A global cyber policy and strategy competition where students respond to a fictional cyber crisis. Learn about the scenario, locations, dates, and how to register for the 2023-25 seasons.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.atlanticcouncil.org/programs/digital-forensic-research-lab/cyber-statecraft-initiative/cyber-912/',
              title: 'Home Page'
            }
          ],
          participation: 4,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      'Cyber FastTrack': {
        aliases: ['Cyber fast Track', 'CyberfastTrack', 'Cyber FastTrack'],
        name: 'Cyber FastTrack',
        properties: {
          description:
            'Cyber FastTrack The program that simplified and accelerated the path to a cybersecurity career for college students. College students increased their security skills and helped advance their learning and careers - whatever their experience level.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.nationalcyberscholarship.org/programs/cyberfasttrack',
              title: 'Home Page'
            }
          ],
          participation: 6,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      'Cyber Panoply': {
        aliases: ['Cyber Panoply', 'panoply'],
        name: 'Cyber Panoply',
        properties: {
          description:
            'Panoply is a timed competition event. At the beginning of the competition, common resources are available for teams to scan, assess, and penetrate. To claim ownership of a service, teams must plant their flag, an assigned hexadecimal string, inside the banner of the service. An automated scanner detects ownership changes and awards ownership of the service to the team whose flag appears in the service banner. At random intervals, an automated scoring engine checks the status and functionality of all critical services. If a team has ownership of a functional critical service during a successful service check, that team is awarded points for owning and maintaining a critical service. Teams accumulate points for each critical service they control and continue to accumulate points as long as they own and maintain those critical services. Teams that fail to secure resources and services they have captured may have them taken away by rival teams. Throughout the competition new resources are added to the common pool, forcing teams to choose between defending existing assets and going after new assets. The team with the highest point total at the end of the competition wins.',
          education_target: ['College'],
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'http://cyberpanoply.com/',
              title: 'Home Page'
            },
            {
              category: 'website',
              description: 'Rules and gameplay description',
              link: 'http://www.cyberpanoply.com/academic.html',
              title: 'Academic Rules'
            }
          ],
          participation: 2,
          practice_available: null,
          qualifiers: false,
          tags: ['King of the Hill', 'Defensive', 'Offensive'],
          teams: '2-4 members',
          virtual: null
        }
      },
      CyberBoat: {
        aliases: ['CyberBoat', 'cyber boat'],
        name: 'CyberBoat',
        properties: {
          description:
            'This event is part of the CyberTransportation Challenges that include CyberAuto and CyberTruck Challenge and has been running since 2012 with attendees from around the world. Come join us for a few days packed with fun, learning, and great contacts in industry, government research, and the hacker community. Our mission is to show college students the exciting and fast-moving world of embedded systems cybersecurity in the transportation domain and in a hands-on setting.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.engr.colostate.edu/~jdaily/CyberBoat/CyberBoatChallenge.html',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      CyberQuest: {
        aliases: ['CyberQuest'],
        name: 'CyberQuest',
        properties: {
          description:
            'CYBERQUEST \u00ae is a cloud-based competition that includes problems created by Lockheed Martin cybersecurity engineers. The Capture the Flag (CTF) format includes multi-step intrusion scenario, steganography, reverse engineering, full OS hacks, packet capture, web exploits, social engineering, and cybersecurity awareness.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.lockheedmartin.com/en-us/who-we-are/communities/cyber-quest.html',
              title: 'Home Page'
            }
          ],
          participation: 3,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF'],
          teams: null,
          virtual: null
        }
      },
      CyberSEED: {
        aliases: ['CyberSEED'],
        name: 'CyberSEED',
        properties: {
          description:
            'CyberSEED is a Capture the Flag (CTF) competition hosted by the Connecticut Cybersecurity Center that challenges college and university students and SYF employees to solve cybersecurity problems. The competition will take place on March 23, 2024 and offer prizes for the top teams.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://cyberskyline.com/events/cyberseed',
              title: 'Home Page'
            }
          ],
          participation: 9,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF'],
          teams: null,
          virtual: null
        }
      },
      CyberTruck: {
        aliases: ['CyberTruck', 'cyber truck'],
        name: 'CyberTruck',
        properties: {
          description:
            'Connecting next generation talent with the heavy duty trucking industry to keep vehicles secure. The CyberTruck Challenge is a premier event to bring together a community of interest related to heavy vehicle cybersecurity issues and develop talent to address those challenges.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.cybertruckchallenge.org/',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: false
        }
      },
      'CyberWarrior CTF': {
        aliases: ['CyberWarrior CTF'],
        name: 'CyberWarrior CTF',
        properties: {
          description:
            'Free CTF Hacking Game By CyberWarrior. An online experience where you acquire cybersecurity skills by finding "flags", hidden bits of data that tell the system you\'ve completed a given task. You\'ll be challenged with games, quizzes and other exercises all designed to introduce you to cybersecurity. Try the CTF today & put your skills to ...',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.cyberwarrior.com/ctf/',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF'],
          teams: null,
          virtual: null
        }
      },
      Cybercup: {
        aliases: ['Cybercup'],
        name: 'Cybercup',
        properties: {
          description:
            'A free, jeopardy-style, CTF competition for students in 4th-12th grade with a focus on cybersecurity themes and challenges. Registration is now closed for the 2024 competition, which will take place on April 3-4, 2024.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://cyber.org/national-cyber-cup',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF'],
          teams: null,
          virtual: null
        }
      },
      Cyberpatriot: {
        aliases: ['Cyberpatriot', 'cyber patriot'],
        name: 'Cyberpatriot',
        properties: {
          description:
            "CyberPatriot's National Youth Cyber Defense Competition challenges teams of high school and middle school students to find and fix cybersecurity vulnerabilities in virtual operating systems. Using a proprietary competition system, teams are scored on how secure they make the system. Top teams advance through the online round of competition, and the best of the best advance to the in-person National Finals Competition.",
          education_target: ['High School', 'Middle School'],
          eligibility: 'Enrolled Students',
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.uscyberpatriot.org/competition/Competition-Overview/competition-overview',
              title: 'Home Page'
            }
          ],
          participation: 7,
          practice_available: true,
          qualifiers: false,
          tags: ['Defensive'],
          teams: '2-6',
          virtual: true
        }
      },
      'Cybersecurity Entrepreneur Challenge': {
        aliases: ['Cybersecurity Entrepreneur Challenge'],
        name: 'Cybersecurity Entrepreneur Challenge',
        properties: {
          description:
            "Venture College, Computer Science, and the Institute for Pervasive Cybersecurity are excited to offer the Cybersecurity Entrepreneur Challenge. This is a 'reverse-pitch' competition where students in higher education, with a solution in the cyber realm, are welcome to compete. If you need inspiration on a cyber problem to tackle, feel free ...",
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.boisestate.edu/venturecollege/cyber_entreprenuer_challenge/',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: ['Entrepreneur'],
          teams: null,
          virtual: null
        }
      },
      Cyberstart: {
        aliases: ['Cyberstart'],
        name: 'Cyberstart',
        properties: {
          description:
            'CyberStart was a game-based cyber security training program that closed in 2023 after 9 years of inspiring learners worldwide. Learn about the game, the team, the programmes and the legacy of this groundbreaking product.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://cyberstart.com/',
              title: 'Home Page'
            }
          ],
          participation: 4,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      Cyphercon: {
        aliases: ['Cyphercon'],
        name: 'Cyphercon',
        properties: {
          description:
            'CypherCon is the largest annual hacker conference in Wisconsin, featuring talks, villages, challenges, and contests on various security topics. The 2025 theme is "Fate or No Fate" and the event will take place on April 3-4 at the Baird "Wisconsin" Center in Milwaukee.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://cyphercon.com/',
              title: 'Home Page'
            }
          ],
          participation: 2,
          practice_available: null,
          qualifiers: null,
          tags: ['Conference'],
          teams: null,
          virtual: null
        }
      },
      DataTribe: {
        aliases: ['DataTribe Challenge', 'DataTribe'],
        name: 'DataTribe',
        properties: {
          description:
            "The DataTribe Challenge is a unique program to accelerate your cybersecurity startup. Workshop your messaging and meet potential investors and customers.  We will pick five finalists to join the program, receive coaching from our team of startup veterans, present at the live event, and benefit from free promotion and press coverage.  Finalists share $25,000 in prizes and all will receive the title of DataTribe Challenge Finalist. Mark your calendars for October 18th, 2024, when we\u2019ll reveal this year\u2019s finalists. We'll begin the messaging workshop with the finalists starting the week of October 21st . The Finalists event is scheduled to take place in the DataTribe Challenge Auditorium in Maple Lawn, Maryland on November 13th, 2024. Applications Deadline: September 27th  (extended to Oct 4)",
          education_target: ['Professional'],
          eligibility: 'Open',
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://datatribe.com/challenge/',
              title: 'Home Page'
            },
            {
              category: 'website',
              description: null,
              link: 'https://datatribe.com/wp-content/uploads/DataTribe-Competition-Terms-and-Conditions-2024-Google-Docs.pdf',
              title: 'Terms and Conditions'
            }
          ],
          participation: 0,
          practice_available: null,
          qualifiers: true,
          tags: ['Entrepreneur'],
          teams: null,
          virtual: null
        }
      },
      DefCon: {
        aliases: ['DefCon'],
        name: 'DefCon',
        properties: {
          description:
            "DEF CON is the world's largest and longest-running hacker convention, held annually in Las Vegas. Find out the latest updates on DEF CON 32, including the theme, venue, calls for papers, workshops, training and more.",
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://defcon.org/?mob=1',
              title: 'Home Page'
            }
          ],
          participation: 7,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF', 'Conference'],
          teams: null,
          virtual: null
        }
      },
      DiceCTF: {
        aliases: ['DICE-CTF'],
        name: 'DiceCTF',
        properties: {
          description:
            'Past Events DiceCTF 2024 Quals. Scoreboard; CTFtime; GitHub; DiceCTF 2023. Scoreboard; CTFtime; GitHub; DiceCTF @ HOPE 2022. Scoreboard; CTFtime; GitHub; DiceCTF 2022',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://ctf.dicega.ng/',
              title: 'Home Page'
            }
          ],
          participation: 3,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF'],
          teams: null,
          virtual: null
        }
      },
      'DigiKey Collegiate Computing Competition': {
        aliases: ['digi-key collegiate computing competition'],
        name: 'DigiKey Collegiate Computing Competition',
        properties: {
          description:
            'The competition will consist of three main events\u2014two programming sessions and one word problem session. ',
          education_target: ['College'],
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.digikey.com/en/resources/edu/dkc3-computing-competition',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: ['Programming'],
          teams: null,
          virtual: null
        }
      },
      'DoD Cyber Sentinel Challenge': {
        aliases: ['DoD Cyber Sentinel Challenge'],
        name: 'DoD Cyber Sentinel Challenge',
        properties: {
          description:
            'Participants will be given a series of real-world challenges that represent the skillsets most in-demand by the DoD. These challenges are designed to measure your understanding of tasks and test your technical, analytical, and strategic abilities in the following skills categories: Forensics, Malware/ Reverse Engineering, Networking & Reconnaissance, Open-Source Intelligence Gathering (OSINT) and Web Security.',
          education_target: ['College', 'Professional'],
          eligibility: 'U.S. citizens',
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.correlation-one.com/dod-cyber-sentinel',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF', 'Forensics', 'Reversing', 'OSINT'],
          teams: null,
          virtual: null
        }
      },
      'DoE CyberFire': {
        aliases: ['Department of Energy CyberFire'],
        name: 'DoE CyberFire',
        properties: {
          description:
            'Cyber Fire events are led by expert security investigators from United States Department of Energy National Laboratories. Our instructors have performed cybersecurity investigation and research continuously since 2005.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://cyberfire.energy.gov/',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      'DoE CyberForce': {
        aliases: ['DoE Cyberforce', 'Department of Energy CyberForce', 'cyberforce'],
        name: 'DoE CyberForce',
        properties: {
          description:
            'CyberForce Competition works to increase 1) hands-on cyber education to college students and professionals, 2) awareness of the nexus between critical infrastructure and cybersecurity,  and  3)  basic  understanding  of  cybersecurity  within  a  real  world scenario. 2024 November 8 @ 11:00 AM - November 9 @ 8:00 PM in St. Charles, IL',
          education_target: ['College'],
          eligibility: 'US Institutions',
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://cyberforce.energy.gov/',
              title: 'Home Page'
            },
            {
              category: 'website',
              description: 'Upcoming events',
              link: 'https://cyberforce.energy.gov/events/',
              title: 'Events'
            },
            {
              category: 'website',
              description: '2024 November 8 @ 11:00 AM - November 9 @ 8:00 PM in St. Charles, IL',
              link: 'https://cyberforce.energy.gov/event/cyberforce-competition/',
              title: 'CyberForce Event Information'
            }
          ],
          participation: 76,
          practice_available: false,
          qualifiers: false,
          tags: ['Red vs Blue', 'Defensive', 'CTF'],
          teams: '4-6',
          virtual: false
        }
      },
      DownUnderCTF: {
        aliases: ['Down Under CTF'],
        name: 'DownUnderCTF',
        properties: {
          description: 'The Largest CTF Down Under!',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://downunderctf.com/',
              title: 'Home Page'
            }
          ],
          participation: 4,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF'],
          teams: null,
          virtual: null
        }
      },
      'Global Collegiate Penetration Testing Competition (CPTC)': {
        aliases: ['Global Collegiate Penetration Testing Competition', 'Collegiate Penetration Testing Competition'],
        name: 'Global Collegiate Penetration Testing Competition (CPTC)',
        properties: {
          description:
            'At its heart, CPTC is a bit different from several other collegiate Cybersecurity competitions. Instead of defending your network, searching for flags, or claiming ownership of systems, CPTC focuses on mimicking the activities performed during a real-world penetration testing engagement conducted by companies, professional services firms, and internal security departments around the world.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://cp.tc/',
              title: 'Home Page'
            }
          ],
          participation: 45,
          practice_available: null,
          qualifiers: null,
          tags: ['Offensive'],
          teams: null,
          virtual: null
        }
      },
      'Google CTF': {
        aliases: ['Google Capture the Flag'],
        name: 'Google CTF',
        properties: {
          description:
            'What is the Google CTF? Google will run the 2023 CTF competition in two parts: an online jeopardy-CTF competition, and a different on-site contest open only to the top 8 teams of the online jeopardy-CTF competition. "Capture The Flag" (CTF) competitions are not related to running outdoors or playing first-person shooters.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'http://capturetheflag.withgoogle.com/',
              title: 'Home Page'
            }
          ],
          participation: 3,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF'],
          teams: null,
          virtual: null
        }
      },
      GrizzHacks: {
        aliases: ['GrizzHacks'],
        name: 'GrizzHacks',
        properties: {
          description:
            'GrizzHacks is a hackathon event where participants build projects from scratch and compete for prizes. Learn about the tracks, schedule, sponsors, FAQs and how to apply for this in- person event on March 9-10th.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.grizzhacks.org/',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      'HTB Cyber Apocalypse': {
        aliases: [
          'Hack the Box: Cyber Apocalypse',
          'HTB Cyber Apocalypse',
          'Cyber Apocalypse',
          'hack the box cyberpocolypse'
        ],
        name: 'HTB Cyber Apocalypse',
        properties: {
          description:
            'By taking part in Cyber Apocalypse you can meet, learn, and compete with the best hackers in the world. Last year, more than 15,000 joined the event. \u26a1 Become etched in HTB history. Making it to the top of the scoreboard means entering officially in a small circle of legendary hackers. Put your name up there and show everyone how real hacking ...',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.hackthebox.com/events/cyber-apocalypse-2024',
              title: 'Home Page'
            }
          ],
          participation: 6,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      'HTB University CTF': {
        aliases: [
          'HTB University CTF',
          'HackTheBox University CTF',
          'Hack The Box University CTF',
          'HTB CTF',
          'Hack the Box UniCTF',
          'hackthebox ctf'
        ],
        name: 'HTB University CTF',
        properties: {
          description:
            "HackTheBox's annual hacking competition for students. Register your university and compete in a themed CTF event!",
          education_target: ['College'],
          eligibility: 'Current Student',
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.hackthebox.com/universities/university-ctf-2023',
              title: 'Home Page'
            }
          ],
          participation: 6,
          practice_available: true,
          qualifiers: false,
          tags: ['CTF', 'Jeopardy', 'Crypto', 'Reversing', 'Forensics', 'Exploitation'],
          teams: '1-30 members',
          virtual: null
        }
      },
      'Hack The Border': {
        aliases: ['Hack The Border'],
        name: 'Hack The Border',
        properties: {
          description:
            "The Binational Hackathon aims to bring together minds from both El Paso, TX, and Cd. Juarez Border to tackle community and border challenges collaboratively. This hackathon offers a non-technical experiential learning opportunity, inviting non-STEM majors to join in the experience. Form a team of up to four members, and if you don't have one, no need to worry\u2014we will help you find the perfect team!",
          education_target: ['College'],
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://hacktheborder.org/Hackathon/main.html',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: ['Conference', 'CTF', 'Forensics', 'Offensive'],
          teams: null,
          virtual: null
        }
      },
      'Hack UNT': {
        aliases: ['Hack UNT'],
        name: 'Hack UNT',
        properties: {
          description:
            'HackUNT is a weekend-long event where hundreds of students from around the country come together to work on excellent new software and/or hardware projects.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.unthackathon.com/',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      'Hack Warz': {
        aliases: ['Hack Warz', 'hackwarz'],
        name: 'Hack Warz',
        properties: {
          description:
            'Hack Warz provides participants with a complex CTF (Capture the flag) ecosystem where both the new and the experienced can learn the art of penetration testing and use existing knowledge to beat out competition. Teams will leverage the tools in Kali Linux to find and exploit vulnerabilities in a wide range of different operating systems. With each successful exploit, teams can redeem tokens found in compromised systems for points.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://pcdc-sc.com/hackwarz.php',
              title: 'Home Page'
            }
          ],
          participation: 2,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF'],
          teams: null,
          virtual: null
        }
      },
      'Hack for Troops CTF': {
        aliases: ['Hack for Troops CTF'],
        name: 'Hack for Troops CTF',
        properties: {
          description:
            'Hack For Troops is an annual Capture the Flag (CTF) competition and fundraising event put on by the Virginia-based non-profit Tech for Troops and PlayCyber, powered by Katzcy.. The purpose of the event is to help raise funds for Tech For Troops, whose mission is dedicated to empowering veterans with computers, skills, and Information Technology (IT) workforce training.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://hackfortroops.playcyber.com/',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF'],
          teams: null,
          virtual: null
        }
      },
      'Hack this site': {
        aliases: ['Hack this site'],
        name: 'Hack this site',
        properties: {
          description:
            'HackThisSite.org offers challenges, CTFs, and more for hackers to test and expand their skills. Join the IRC, Discord, forums, and GitHub to learn, share, and get involved with the project.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.hackthissite.org/',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF'],
          teams: null,
          virtual: null
        }
      },
      'Hack-a-Sat': {
        aliases: ['Hack The Sat', 'HackASat', 'Hack-a-Sat'],
        name: 'Hack-a-Sat',
        properties: {
          description:
            "Hack-A-Sat 4 is the world's first CTF competition in space, where five finalist teams compete on Moonlighter, an on-orbit satellite. Learn more about the game, the teams, the challenges, the scoring, the videos and the open source release.",
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://hackasat.com/',
              title: 'Home Page'
            }
          ],
          participation: 3,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF'],
          teams: null,
          virtual: null
        }
      },
      'Hack@SCHack': {
        aliases: ['Hack@SCHack'],
        name: 'Hack@SCHack',
        properties: {
          description:
            'The Hack@SChack Hackathon is a Capture-the-Flag event to challenge yourself, stretch your security muscle, and have FUN! CyberQ is a cyber range tactical range environment consisting of a virtual machines and tools to support red team and blue team environments. This range technology was developed by EC-Council, the inventors of Certified ...',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://hackaschack.ati.org/',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      HackKean: {
        aliases: ['HackKean', 'hack kean'],
        name: 'HackKean',
        properties: {
          description:
            "Kean University's Annual Hackathon. Join us for 12 hours online as we build apps, hardware, and more. We will provide a comfortable and enlivening atmosphere for you to build out progressive, innovative, and crazy ideas. At HackKean, you'll meet fellow hackers, learn new technologies, and hone your skills. We'll have free prizes, games, and more.",
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://hackkean.org/',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      'HackPack CTF': {
        aliases: ['HackPack CTF'],
        name: 'HackPack CTF',
        properties: {
          description:
            'Hosted by North Carolina State University. This competition is a Jeopardy-style CTF, which means that challenges are independent and run on our infrastructure. This year, we are not following the traditional categories (pwn,re,web,etc), but instead are introducing a new set of categories that would be revealed on the day of the competition.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://hackpack.club/ctf2024/',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF'],
          teams: null,
          virtual: null
        }
      },
      HackSMU: {
        aliases: ['HackSMU'],
        name: 'HackSMU',
        properties: {
          description:
            'HackSMU is a 24-hour educational event that combines a tech conference, a career fair, and a start-up competition. Come learn new skills, meet corporate recruiters, create amazing projects, and have fun at HackSMU!',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://hacksmu.org/',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      'Hacky Holidays': {
        aliases: ['Hacky Holidays'],
        name: 'Hacky Holidays',
        properties: {
          description:
            'Hacky Holidays is an online Capture The Flag game where you can test and improve your skills with all sorts of challenges (technical puzzles) related to cyber security and emerging technologies. Each of the challenges has a task description which give you a clue on how you can solve the particular challenge. An example challenge could be a web ...',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www2.deloitte.com/nl/nl/pages/risk/articles/hacky-holidays.html',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      HeroCTF: {
        aliases: ['HeroCTF'],
        name: 'HeroCTF',
        properties: {
          description:
            'HeroCTF is an online cybersecurity competition for beginners and intermediates that takes place once a year. All the challenges source code and write-ups are uploaded on our Github. HeroCTF V6 - Online cybersecurity competition - October 2024',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.heroctf.fr/',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF', 'Forensics', 'Reversing'],
          teams: '5 members',
          virtual: null
        }
      },
      Hivestorm: {
        aliases: ['Hive storm', 'Hivestorm'],
        name: 'Hivestorm',
        properties: {
          description:
            'Hivestorm is a cyber defense competition where teams secure virtual machines and score points by removing malware and other threats. Teams consist of 2 to 4 players, must have their own equipment and Internet connectivity, and can practice security, forensic, and system administration skills.',
          education_target: ['College'],
          eligibility: 'Registered Student',
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'http://hivestorm.org/',
              title: 'Home Page'
            }
          ],
          participation: 28,
          practice_available: true,
          qualifiers: false,
          tags: ['Defensive'],
          teams: '2-4 members',
          virtual: null
        }
      },
      'ICL Collegiate Cup': {
        aliases: ['ICL', 'ICL Collegiate Cup', 'International Cyber League Collegiate Cup'],
        name: 'ICL Collegiate Cup',
        properties: {
          description:
            'The ICL Collegiate Cup allows students in cyber security programs to compete in live-fire challenges that mirror the scenarios they will face on the job in a hyper-realistic cyber range that includes enterprise-grade networks and commercial security tools. The Collegiate Cup is a prime opportunity for students to prove that they have what it ...',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://icl.cyberbit.com/',
              title: 'Home Page'
            }
          ],
          participation: 15,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      IEEEXtreme: {
        aliases: ['IEEEXtreme'],
        name: 'IEEEXtreme',
        properties: {
          description:
            'IEEEXtreme is a global challenge in which teams of IEEE Student members \u2013 advised and proctored by an IEEE member, and often supported by an IEEE Student Branch \u2013 compete in a 24-hour time span against each other to solve a set of programming problems.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://ieeextreme.org/',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: true,
          qualifiers: null,
          tags: ['Programming'],
          teams: null,
          virtual: null
        }
      },
      'ISACA North Texas Student Case Competition': {
        aliases: ['ISACA North Texas Student Case Competition'],
        name: 'ISACA North Texas Student Case Competition',
        properties: {
          description:
            'The ISACA North Texas Case Competition provides students with a platform to apply their knowledge and skills to real-world business problems, enhancing their learning experiences while preparing them for future success in the field of cybersecurity.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: 'ISACA North Texas Chapter event calendar',
              link: 'https://engage.isaca.org/northtexaschapter/events/recentcommunityeventsdashboard',
              title: 'Event Calendar'
            },
            {
              category: 'website',
              description: null,
              link: 'https://engage.isaca.org/northtexaschapter/events/eventdescription?CalendarEventKey=1f64a6ce-7e7b-41e1-a81f-0187c32c4d44&CommunityKey=216d5192-1574-4421-9bfa-6b470d758889&Home=%2Fevents%2Fcalendar',
              title: '2023 Event'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      'ISEaGE Cyber Defense Competition': {
        aliases: ['ISEaGE Cyber Defense Competition'],
        name: 'ISEaGE Cyber Defense Competition',
        properties: {
          description:
            'Home. Your place for information about Cyber Defense Competitions at Iowa State University! Spring 2016 ISU CDC in Coover Hall.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://cdc.iseage.org/',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      ISTS: {
        aliases: ['ISTS', 'RITSEC ISTS', 'rit information security talent search'],
        name: 'ISTS',
        properties: {
          description:
            'Each year, colleges from around the country compete for the coveted title of ISTS champion. Competitors are faced with a wide variety of challenges which are designed to cover as many facets of computing security, system administration, networking, and programming as we possibly can. These challenges include code review, architecture design, incident response, and policy writing -- all while defending a completely student-built infrastructure.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://ists.io/',
              title: 'Home Page'
            },
            {
              category: 'registration',
              description: null,
              link: 'https://ists.io/register',
              title: 'Registration'
            }
          ],
          participation: 8,
          practice_available: null,
          qualifiers: null,
          tags: ['Red vs Blue', 'Defensive'],
          teams: null,
          virtual: null
        }
      },
      'ISU CDC': {
        aliases: ['ISU runs 3 college level CDC per year', 'National CDC (Iowa State)'],
        name: 'ISU CDC',
        properties: {
          description:
            'Cyber Defense Competitions. Iowa State University hosts five Cyber Defense Competitions (CDC) each year. CDCs are held on a Friday and Saturday in 1313 Coover Hall. Friday is for final set up and the actual competition starts at 8 a.m. on Saturday. The scenario for the competition is released about four weeks prior to the event so that teams ...',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.cyio.iastate.edu/cyber-defense-competitions/',
              title: 'Home Page'
            }
          ],
          participation: 2,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      'IT-Olympics': {
        aliases: ['ITOlympics'],
        name: 'IT-Olympics',
        properties: {
          description:
            "IT-Olympics is held on the Iowa State University campus and showcases students' IT talents and knowledge in competitions and presentations. Teams from across Iowa compete in three venues: Cyber Defense, Robotics, and Smart-IT. These areas of focus correspond with the educational materials and challenges that students have worked on throughout ...",
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'http://www.it-adventures.org/it-o/',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      'Insidious Hackathon': {
        aliases: ['Insidious Hackathon'],
        name: 'Insidious Hackathon',
        properties: {
          description:
            "INSIDIOUS is a hackathon training event brought to you by Nike's Corporate Information Security team. Students will compete in a safe, sandboxed environment where they will seek out and take advantage of vulnerabilities in a real application. INSIDIOUS, with the theme Ready Player One, will be held Thursday, March 3, 2022, to Thursday, March 10, 2022, with four sessions to choose from. Registration closes Monday, February 28, 2022.",
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://innercircle.engineering.asu.edu/2022/02/take-on-the-insidious-hackathon-by-nike-march-3-10-learn-more-february-16-and-23/',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      'International Collegiate Programming Contest': {
        aliases: ['International Collegiate Programming Contest', 'icpc'],
        name: 'International Collegiate Programming Contest',
        properties: {
          description:
            'An online (distributed) programming contest, offered as a drop-in replacement for so-called \u201cqualifying\u201d contests (e.g., school-level, pre-regional competitions). ICPC is the oldest, largest, and most prestigious programming contest in the world. Teams of three from over 3,000 universities in 111 countries compete in local, regional, and world finals to solve real-world problems and showcase their skills.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://na.icpc.global/',
              title: 'Home Page'
            }
          ],
          participation: 4,
          practice_available: true,
          qualifiers: true,
          tags: ['Programming'],
          teams: null,
          virtual: null
        }
      },
      JerseyCTF: {
        aliases: ['JerseyCTF', 'Jersey CTF'],
        name: 'JerseyCTF',
        properties: {
          description:
            'JerseyCTF is a beginner-friendly Capture the Flag competition that aims to inspire interest in cybersecurity. Organized by the NJIT Association for Computing Machinery, the NJIT Information and Cybersecurity Club, and the NJIT SCI Students program, it is geared towards students, beginners, and professionals alike. JerseyCTF provides ...',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.jerseyctf.com/',
              title: 'Home Page'
            }
          ],
          participation: 9,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF'],
          teams: null,
          virtual: null
        }
      },
      'Knight CTF': {
        aliases: ['Knight CTF'],
        name: 'Knight CTF',
        properties: {
          description:
            'Then, in 2022, Knight Squad took a bold step forward on their path. With a sense of duty to bolster the global cybersecurity community, they introduced the KnightCTF - the first international Capture The Flag event to emerge from the heart of Bangladesh. With their inaugural CTF, they embarked on a mission to showcase the remarkable talents ...',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.knightctf.com/',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF'],
          teams: null,
          virtual: null
        }
      },
      KringleCon: {
        aliases: ['kringlecon', 'SANS - Kris Kringle Challenge', 'sans krinklecon ctf'],
        name: 'KringleCon',
        properties: {
          description:
            'Join the global cybersecurity community in its most festive cybersecurity challenge of the year. The SANS Holiday Hack Challenge is a FREE series of super fun, high-quality, hands-on cybersecurity challenges. The SANS Holiday Hack Challenge is for all skill levels, with a prize at the end for the best of the best entries.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.sans.org/mlp/holiday-hack-challenge-2023/',
              title: 'Home Page'
            }
          ],
          participation: 3,
          practice_available: null,
          qualifiers: null,
          tags: ['Conference'],
          teams: null,
          virtual: null
        }
      },
      'LA CTF': {
        aliases: ['LA CTF'],
        name: 'LA CTF',
        properties: {
          description:
            'Store your data locally and access it from anywhere without subscription fees! UCLA students, open a ticket in the LA CTF discord for a 30% discount. #WeAreCrowdStrike and our mission is to stop breaches. Since our inception, our market leading cloud- native platform has offered unparalleled protection against the most sophisticated cyberattacks.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.lac.tf/',
              title: 'Home Page'
            }
          ],
          participation: 3,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF'],
          teams: null,
          virtual: null
        }
      },
      'Long Island CTF': {
        aliases: ['Long Island Capture the Flag Competition'],
        name: 'Long Island CTF',
        properties: {
          description:
            '(ISC)2 Long Island Chapter is Proud to Announce the Second Annual Long Island Capture The Flag (CTF) or also known as Hack-A-Thon. Originally started in 2022 and administered by the Education Committee, was a great success. Agenda 9am Registration and Breakfast 10am Event Rules Briefing 10:15 Event Start 1pm Lunch 3pm Competition Ends / Judging [\u2026]',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://isc2chapter-liny.org/events/isc2-long-islands-second-annual-long-island-capture-the-flag/',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF'],
          teams: null,
          virtual: null
        }
      },
      'MAGIC CTF': {
        aliases: ['MAGIC CTF'],
        name: 'MAGIC CTF',
        properties: {
          description:
            "Learn the basics of cybersecurity skills and teamwork in a fun and challenging environment with MAGIC's Capture the Flag events. Participate in beginner-level puzzles that require hacking tools, coding, and logic to find the flags and win the competition.",
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://magicinc.org/programs/cybersecurity/capture-the-flag',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF'],
          teams: null,
          virtual: null
        }
      },
      'MGA CTF': {
        aliases: ['MGA CTF'],
        name: 'MGA CTF',
        properties: {
          description:
            'CTF problems are asked in job interviews to test the skills of professionals. So, competing in CTF competitions may prepare you for cybersecurity job interviews as well. For a more detailed explanation, with examples, watch this video below to go deeper into what CTF Challenges are and what one may experience when trying to complete an exercise.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://studentweb.mga.edu/randall.callihan/mga_cyberknights/ctf-training.html',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF'],
          teams: null,
          virtual: null
        }
      },
      MHACKS: {
        aliases: ['MHACKS'],
        name: 'MHACKS',
        properties: {
          description:
            "About Google x MHacks. MHacks is the University of Michigan's premier student-run hackathon. This April 12th to 14th, we are collaborating with Google to host Google x MHacks, the Google AI Hackathon, at the Central Campus Classroom Building (CCCB).A few workshops, led by Google engineers and PMs, will be hosted on Friday the 12th, while the hackathon will take place Saturday and Sunday.",
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.mhacks.org/about',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      'MISI Hack The Building: Hospital Edition': {
        aliases: ['Hack-the-hospital', 'hack the hospital', 'MISI Hack The Building', 'Hack the Building'],
        name: 'MISI Hack The Building: Hospital Edition',
        properties: {
          description:
            'The Hack The Building 2.0 Hospital Edition and the National Cybersecurity Education Colloquium (NCEC) are two concurrent cybersecurity events by MISI (Now TAC) & DreamPort (Now TheLink) & The National Centers of Academic Excellence in Cybersecurity (NCAE-C) program. This conference took place on September 18-22, 2023.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://thetac.tech/hack-the-building-2-hospital-edition/',
              title: 'Home Page'
            }
          ],
          participation: 11,
          practice_available: null,
          qualifiers: null,
          tags: ['Conference'],
          teams: null,
          virtual: null
        }
      },
      'MISI Hack The Port': {
        aliases: ['MISI Hack The Port', 'Hack-the-port by MISI', 'hack the port'],
        name: 'MISI Hack The Port',
        properties: {
          description:
            'Stay tuned for Hack The Port 2023 returning to Florida. Please fill out the form below to get updated on DreamPort and MISI events like Hack The Port! The Maritime and Control Systems Cybersecurity Con - Hack the Port 22 is a hybrid cybersecurity event held in Ft. Lauderdale, Florida on March 21-25 at the Greater Ft. Lauderdale Convention ...',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.hacktheport.tech/',
              title: 'Home Page'
            }
          ],
          participation: 38,
          practice_available: null,
          qualifiers: null,
          tags: ['Conference'],
          teams: null,
          virtual: null
        }
      },
      'Maple CTF': {
        aliases: ['Maple CTF', 'maple bacon ctf'],
        name: 'Maple CTF',
        properties: {
          description:
            "Maple Bacon's annual CTF competition. about getting_started blog members challenge maplectf_2023. Maple Bacon. CTF Team at the University of British Columbia Thanks for playing! MapleCTF 2022.",
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://ctf.maplebacon.org/',
              title: 'Home Page'
            }
          ],
          participation: 2,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF'],
          teams: null,
          virtual: null
        }
      },
      MetaCTF: {
        aliases: ['Meta CTF', 'MetaCTF'],
        name: 'MetaCTF',
        properties: {
          description:
            'This CTF will challenge participants of all skill levels to learn new cybersecurity techniques and skills, with problems covering a variety of topics such as web exploitation, cryptography, binary exploitation, reverse engineering, forensics, and reconnaissance.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://app.metactf.com/cybergames',
              title: 'Home Page'
            }
          ],
          participation: 3,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF'],
          teams: null,
          virtual: true
        }
      },
      'Mountain West Cyber Challenge': {
        aliases: [
          'Deloitte Capture the Flag',
          'Deloitte Mt. West Cyber Challenge',
          'Mountain West CTF',
          'mountain west cybersecurity competition',
          'mwcc cyber challenge'
        ],
        name: 'Mountain West Cyber Challenge',
        properties: {
          description:
            "Teams will compete in one 7-hour round of Cybersecurity Capture the Flag (CTF) to take home the title and great prizes! Teams can be geographically collocated but the challenge event will be 100% online. Space is limited and will be on a first-come, first-served basis. No cost to compete!. This event will be hosted on Deloitte's Cyber CTF platform, Hackazon. with now over 400 scenarios on the platform, you are sure to be faced with some great challenges. The 6th Annual Mountain West Cyber Challenge (MWCC) will be held this December 6th, 2025.",
          education_target: ['College'],
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://cybersecurity.uccs.edu/events/mountain-west-cybersecurity-challenge-mwcc',
              title: 'Home Page'
            },
            {
              category: 'registration',
              description: null,
              link: 'https://web.cvent.com/event/c0c22e6a-56b9-4514-818d-2159bbe57ca4/summary',
              title: 'Registration 2024'
            },
            {
              category: 'website',
              description: null,
              link: 'https://us.hackazon.org/#!/public_timeline/25/f166059ae4b5325f571fa53934ea200f',
              title: 'Hackazon Leaderboard'
            }
          ],
          participation: 8,
          practice_available: false,
          qualifiers: false,
          tags: [
            'CTF',
            'OSINT',
            'Crypto',
            'Networking',
            'Recon',
            'Forensics',
            'Exploitation',
            'Web',
            'Reversing',
            'Programming'
          ],
          teams: 'up to 4',
          virtual: true
        }
      },
      'NATO Locked Shields': {
        aliases: ['Locked Shields'],
        name: 'NATO Locked Shields',
        properties: {
          description:
            'It is a Red team vs. Blue Team exercise, where the latter are formed by member nations of CCDCOE. In 2021 there were 22 Blue Teams participating with an average 40 experts in each team. The Teams take on the role of national cyber Rapid Reaction Teams that are deployed to assist a fictional country in handling a large-scale cyber incident with all its implications. The Exercise in 2021 involved about 5000 virtualised systems that were subject to more than 4000 attacks. The teams must be effective in reporting incidents, executing strategic decisions and solving forensic, legal and media challenges. To keep up with technology developments, Locked Shields focuses on realistic scenarios and cutting-edge technologies, relevant networks and attack methods.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://ccdcoe.org/exercises/locked-shields/',
              title: 'Home Page'
            }
          ],
          participation: 2,
          practice_available: null,
          qualifiers: null,
          tags: ['Red vs Blue', 'Defensive'],
          teams: null,
          virtual: null
        }
      },
      'NCAE Cyber Games': {
        aliases: ['Cyber Games 2023 by NCAE'],
        name: 'NCAE Cyber Games',
        properties: {
          description:
            'NCAE Cyber Games is a national competition for college students who have never competed in cybersecurity before. It aims to inspire, educate, and support newcomers to the field and increase the diversity of the cyber workforce. Regional events are virtual, invitational event is hosted physically.',
          education_target: ['Early College'],
          eligibility: 'CAE Institutions',
          instances: [],
          links: [
            {
              category: 'website',
              description: 'General information and registration',
              link: 'https://www.ncaecybergames.org/',
              title: 'Home Page'
            },
            {
              category: 'training',
              description: 'Test your skills and prepare for the NCAE Cyber Games in the Sandbox!',
              link: 'https://ui.sandbox.ncaecybergames.org/',
              title: 'NCAE Sandbox'
            },
            {
              category: 'website',
              description: 'Find a competition date',
              link: 'https://ncaecybergames.org/schedule/',
              title: 'Schedule'
            }
          ],
          participation: 113,
          practice_available: true,
          qualifiers: false,
          tags: ['Red vs Blue', 'Defensive', 'CTF'],
          teams: '3-10 members',
          virtual: true
        }
      },
      NICCDC: {
        aliases: ['NIATEC Intercollegiate Cyber Defense Competition', 'NICCDC'],
        name: 'NICCDC',
        properties: {
          description:
            'The NICCDC is designed to stimulate mission-oriented decision making through unique and immersive scenarios that score decision making over technical abilities alone. Find Out More; What to Help Out? Every year we need volunteers and industry professionals to help pass on their knowledge and skills to students in our program and in our ...',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://niccdc.niatec.iri.isu.edu/',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: ['Red vs Blue', 'Defensive'],
          teams: null,
          virtual: null
        }
      },
      'NSA Codebreaker': {
        aliases: ['NSA Codebreaker'],
        name: 'NSA Codebreaker',
        properties: {
          description:
            "The NSA Codebreaker Challenge provides students with a hands-on opportunity to develop their reverse-engineering / low-level code analysis skills while working on a realistic problem set centered around the NSA's mission. While the challenge is intended for students, professors are encouraged to participate as well. NSA Codebreaker Challenge 2024: September 16th, 2024 - January 17th, 2025!",
          education_target: ['Middle School', 'High School', 'College'],
          eligibility: 'US Institutions',
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://nsa-codebreaker.org/home',
              title: 'Home Page'
            },
            {
              category: 'website',
              description: 'Tools, previous challenges, lectures and talks!',
              link: 'https://nsa-codebreaker.org/resources',
              title: 'Resources'
            },
            {
              category: 'website',
              description: null,
              link: 'https://nsa-codebreaker.org/FAQ',
              title: 'FAQ'
            }
          ],
          participation: 84,
          practice_available: true,
          qualifiers: false,
          tags: ['CTF', 'Reversing', 'Exploitation'],
          teams: 'optional',
          virtual: true
        }
      },
      'NSA Cyber Exercise': {
        aliases: ['CAE-NCX', 'NSA-NCX', 'NCX', 'NCAX', 'NSA Cyber Exercise'],
        name: 'NSA Cyber Exercise',
        properties: {
          description:
            'Cybersecurity is a crucial component of a strong national defense. NSA helps educate, train, and test the cyber skills of U.S. service academy cadets and midshipmen teams from the senior military colleges to become future cyber warriors and leaders through its annual NSA Cyber Exercise (NCX). We help ensure both future military active duty ...',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.nsa.gov/Cybersecurity/NSA-Cyber-Exercise/',
              title: 'Home Page'
            }
          ],
          participation: 14,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      'NUARI TableTop': {
        aliases: ['NUARI tabletop'],
        name: 'NUARI TableTop',
        properties: {
          description:
            'Gain greater understanding of key roles and responsibilities during executive cyber response. Gain an increased understanding of the complexities of an executive cyber response.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://c4cyi.cityu.edu/center/competitions-ay-2023-2024/nuari/',
              title: '2023 Event'
            }
          ],
          participation: 2,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      'National Cyber League': {
        aliases: ['National Cyber League', 'NCL', 'National Cyber League (NCL)'],
        name: 'National Cyber League',
        properties: {
          description:
            'The NCL is a virtual platform for students to develop and showcase their cybersecurity skills through practical exercises and labs. It also connects students with employers and educators across the country. NCL Fall 2024 Season Schedule: Registration is open August 19 - October 11 and costs $35 to participate. Late registration is October 12 - 15 with a cost increase to $45. To register for the NCL season, you must be at least 13 years old and a student enrolled in a high school, collegiate institution, apprenticeship, or academic boot camp. NCL is currently only available to residents in the United States and Canada and their territories.',
          education_target: ['High School', 'College'],
          eligibility: 'Open',
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://nationalcyberleague.org/',
              title: 'Home Page'
            },
            {
              category: 'website',
              description: null,
              link: 'https://nationalcyberleague.org/competition',
              title: 'Competition'
            },
            {
              category: 'website',
              description: null,
              link: 'https://nationalcyberleague.org/competition/faq',
              title: 'Competition FAQ'
            },
            {
              category: 'training',
              description: 'Resources to lean and prepare',
              link: 'https://nationalcyberleague.org/competition/resources',
              title: 'Competition Resources'
            }
          ],
          participation: 239,
          practice_available: true,
          qualifiers: false,
          tags: ['CTF', 'OSINT', 'Crypto', 'Networking', 'Recon', 'Forensics', 'Exploitation', 'Web'],
          teams: '1-7 members',
          virtual: true
        }
      },
      'NeverLAN CTF': {
        aliases: ['NeverLAN CTF'],
        name: 'NeverLAN CTF',
        properties: {
          description:
            'The NeverLAN CTF is an international Capture The Flag event aimed towards teaching computer science to our younger generation.',
          education_target: ['Middle School'],
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.neverlanctf.com/',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF', 'Crypto', 'Programming'],
          teams: null,
          virtual: null
        }
      },
      "NorCal Mayor's Cup": {
        aliases: ["NorCal Mayor's Cup", 'California Mayors Cyber Cup'],
        name: "NorCal Mayor's Cup",
        properties: {
          description:
            'The NorCalCyber Mayors Cup brings awareness to cybersecurity careers and provides students with an opportunity to engage in team-based learning. The Sacramento County Office of Education has support ed students and their coaches to participate in the NorCalCyber Mayors Cup since 2018.. In 2018, the California State Assembly issued a challenge: Fill the 40,000 open cybersecurity jobs ...',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.norcalcyber.org/',
              title: 'Home Page'
            }
          ],
          participation: 2,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      'North Carolina Agricultural and Technical State University HACKNCAT': {
        aliases: ['HACKNCAT'],
        name: 'North Carolina Agricultural and Technical State University HACKNCAT',
        properties: {
          description: 'North Carolina Agricultural and Technical State University annual hackathon',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.ncat.edu/calendar/2022/04/semi-annual-hackathon.php',
              title: '2022 Event'
            },
            {
              category: 'website',
              description: null,
              link: 'https://www.ncat.edu/coe/college-and-department-events/hackncat20.php',
              title: '2020 Event'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      'Palmetto Cyber Defense Contest National Cyber Exercise': {
        aliases: ['Palmetto Cyber Defense', 'Palmetto Cyber Defense Contest National Cyber Exercise'],
        name: 'Palmetto Cyber Defense Contest National Cyber Exercise',
        properties: {
          description:
            "PCDC is an event for the promotion of Cyber Security education and awareness. Competition energizes local high schools and colleges to develop invigorating and focused curriculum for the technical needed in today's fast-paced and challenging cyber environment. A brochure for the event is posted in the Documents Section.",
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'http://pcdc-sc.com/',
              title: 'Home Page'
            }
          ],
          participation: 5,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      'Palo Alto Secure the Future': {
        aliases: ['Palo Alto Secure the Future', 'paloalto securing the future'],
        name: 'Palo Alto Secure the Future',
        properties: {
          description:
            'This academic competition is designed to challenge actively enrolled students in U.S. colleges and Universities how to make decisions regarding protection of operational assets through the analysis, comparison, and selection of advanced security tools, methodologies, and implementation options. Palo Alto Networks is excited to kick off the 2023-2024 Secure the Future competition and encourage all interested students to apply before October 13.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.paloaltonetworks.com/blog/2023/10/secure-the-future/',
              title: 'Home Page'
            },
            {
              category: 'website',
              description: null,
              link: 'https://docs.google.com/document/d/1bNg38iJmRD1FPvAkjzAnQBTQYWzuKegm/edit',
              title: '2023 Competition Overview'
            }
          ],
          participation: 2,
          practice_available: null,
          qualifiers: true,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      PicoCTF: {
        aliases: ['pico-CTF', 'pico CTF', 'picoCTF', 'Pico CTF', 'PicoCTF'],
        name: 'PicoCTF',
        properties: {
          description:
            "Support Free Cybersecurity Education. picoCTF relies on generous donations to run. So far we've reached over 350,000 learners across the world. Consider or. picoCTF is a free computer security education program with original content built on a capture-the-flag framework created by security and privacy experts at Carnegie Mellon University.",
          education_target: ['High School'],
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.picoctf.org/',
              title: 'Home Page'
            }
          ],
          participation: 16,
          practice_available: true,
          qualifiers: false,
          tags: ['CTF'],
          teams: 'optional',
          virtual: true
        }
      },
      "President's Cup": {
        aliases: ["President's Cup"],
        name: "President's Cup",
        properties: {
          description:
            'Cyber threats across the globe have put into focus our country\u2019s need for cyber talent. CISA leads and hosts the President\u2019s Cup Cybersecurity Competition to identify, recognize, and reward the best cyber talent across the federal workforce. Participants are challenged to outthink and outwit their competitors in a series of tests that are based on real-world situations to expand cyber skills.',
          education_target: ['Professional'],
          eligibility: 'Federal workforce',
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.cisa.gov/presidents-cup-cybersecurity-competition',
              title: 'Home Page'
            },
            {
              category: 'training',
              description: 'Try out previous challenges!',
              link: 'https://presidentscup.cisa.gov/gb/practice',
              title: 'Practice Arena'
            },
            {
              category: 'website',
              description: "President's Cup Cybersecurity Competition Challenges and Guides",
              link: 'https://github.com/cisagov/prescup-challenges/tree/main',
              title: 'Github - Challenges'
            }
          ],
          participation: 3,
          practice_available: true,
          qualifiers: false,
          tags: [],
          teams: 'optional',
          virtual: true
        }
      },
      PwnTillDawn: {
        aliases: ['onlinepwntilldawn.com'],
        name: 'PwnTillDawn',
        properties: {
          description:
            'PwnTillDawn Online is a platform for offensive security and penetration testing, where you can learn and test your skills in a realistic and fun environment. It is a collaboration of wizlynx group and capture-the-flag competitions, where you can compete with other players and teams.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://online.pwntilldawn.com/',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      'RM AFCEA CTF': {
        aliases: ['Rocky Mountain AFCEA CTF'],
        name: 'RM AFCEA CTF',
        properties: {
          description:
            'Join us for the sixth annual cyber Capture the Flag (CTF) Challenge at the AFCEA Rocky Mountain Cyberspace Symposium 2022, February 21st -24th, 2022. We have High School- and College- level competitions to take home the Symposium Title and the CTF Flag! Our RMCS22 presenting sponsor, Deloitte, will again provide the Hackazon CTF Arena as we ...',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://crisp.cs.du.edu/news/afcea-ctf-2022/',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF'],
          teams: null,
          virtual: null
        }
      },
      'RMCS CTF': {
        aliases: [
          'Rocky Mountain Cyberspace Symposium',
          'Rocky Mountain Cyberspace Symposium 2023 Collegiate Capture the Flag'
        ],
        name: 'RMCS CTF',
        properties: {
          description:
            'Join us for the sixth annual cyber Capture the Flag (CTF) Challenge at the AFCEA Rocky Mountain Cyberspace Symposium 2022, February 21st -24th, 2022. We have High School- and College- level competitions to take home the Symposium Title and the CTF Flag! Our RMCS22 presenting sponsor, Deloitte, will again provide the Hackazon CTF Arena as we ...',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://crisp.cs.du.edu/news/afcea-ctf-2022/',
              title: 'Home Page'
            }
          ],
          participation: 2,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF'],
          teams: null,
          virtual: null
        }
      },
      RUSecure: {
        aliases: ['Radford University RUSecure', 'RU Secure', 'RUSecure'],
        name: 'RUSecure',
        properties: {
          description:
            'A team-based cybersecurity competition for students from Virginia schools. Registration is open and the Contest will begin on February 29th. Top teams will qualify for the Final Round.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://rusecurectf.radford.edu/',
              title: 'Home Page'
            }
          ],
          participation: 6,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      'Raymond James CTF': {
        aliases: ['Raymond James Invitational CTF', 'Raymond James CTF'],
        name: 'Raymond James CTF',
        properties: {
          description: 'Raymond James hosts an invite-only CTF competition.',
          education_target: null,
          eligibility: 'Invite Only',
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.raymondjames.com/careers/all-access/campus-recruiting/capture-the-flag-event',
              title: '2022 Event'
            }
          ],
          participation: 5,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF'],
          teams: null,
          virtual: null
        }
      },
      'RevolutionUC Hackathon': {
        aliases: ['Revolution Hackathon'],
        name: 'RevolutionUC Hackathon',
        properties: {
          description:
            "RevolutionUC is a 24-hour in-person student hackathon at the University of Cincinnati that is organized by ACM@UC.We invite you to join 300+ motivated students for an awesome weekend of code, community, and self-improvement! You don't have to have to be a computer science major or engineering student to attend.",
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://revolutionuc.com/',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      'SAE CyberAuto': {
        aliases: ['SAE CyberAuto Challenge', 'cyber auto'],
        name: 'SAE CyberAuto',
        properties: {
          description:
            'The CyberAuto Challenge is the oldest and longest running event that focuses on automotive cybersecurity (established in 2012). It seeks to inspire and train the next generation workforce and puts students in teams with industry, government, and hackers/researchers to work on real platforms and full systems. Trainers are among the best in the ...',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.cyberauto-challenge.org/',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      'SANS Holiday Hack Challenge': {
        aliases: ['SANS Holiday Hack Challenge'],
        name: 'SANS Holiday Hack Challenge',
        properties: {
          description:
            'The SANS Holiday Hack Challenge is a FREE series of super fun, high-quality, hands-on cybersecurity challenges. The SANS Holiday Hack Challenge is for all skill levels, with a prize at the end for the best of the best entries. Play Now! Play Now 2023 Winners 2023 Talks Merch Music Credits Sponsors Fun Fact Explore More Connect.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.sans.org/mlp/holiday-hack-challenge-2023/',
              title: 'Home Page'
            }
          ],
          participation: 2,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      'SANS NetWars': {
        aliases: ['SANS NetWars', 'SANS NetWars Academy Cup'],
        name: 'SANS NetWars',
        properties: {
          description:
            'The SANS Institute offers a series of challenge types through their NetWars modules. These challenges are available for a wide variety of skill levels, and even feature a miniaturized physical city over which challenge participants can attempt to compete for the cyber resources. A wide range of competitions are available throughout the year in locations around the world.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.sans.org/netwars',
              title: 'Home Page'
            },
            {
              category: 'website',
              description: null,
              link: 'https://www.sans.org/cyber-ranges/#upcoming-cyber-ranges',
              title: 'Upcoming Tournaments'
            }
          ],
          participation: 3,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: '1-5',
          virtual: null
        }
      },
      'SIGPwny UIUCTF': {
        aliases: ['SIGPwny'],
        name: 'SIGPwny UIUCTF',
        properties: {
          description:
            'UIUCTF is one of the largest academic security CTF competitions, with 1,000+ teams and 2,000+ players from the global cybersecurity community competing last year. SIGPwny, the computer security club at UIUC, is seeking sponsors to help run our next event taking place in Summer 2023! Sponsor T iers Perks 0x00 $10,000+ 0x01 $5,000+ 0x02',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://sigpwny.com/uiuctf',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF'],
          teams: null,
          virtual: null
        }
      },
      'STEM CTF': {
        aliases: ['Capitol STEM Day CTF'],
        name: 'STEM CTF',
        properties: {
          description:
            'In support of STEM outreach, MITRE hosts an annual Capture the Flag (CTF) competition. High school and undergraduate college students have the opportunity to compete in cybersecurity challenges to gain real-life experience and win scholarships.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://mitrecyberacademy.org/compete',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF'],
          teams: null,
          virtual: null
        }
      },
      SkillsUSA: {
        aliases: ['SkillUSA', 'SkillsUSA', 'Skills USA'],
        name: 'SkillsUSA',
        properties: {
          description:
            "The competition is open to active SkillsUSA members enrolled in programs with Cyber Security, Information Security, or Systems and Networking Security Architecture as occupational objectives. Students will be tested on the elements of the NIST Publication 800-181 Cybersecurity Workforce Framework categories including Securely Provision, Operate and Maintain, and Protect and Defend. This competition's skill performance stations are created to be part of a \"scouting combine\" where teams will demonstrate a wide range of skillsets needed in Cyber Security industry. This scouting combine will assess a team's knowledge and skills in a series of individual stations that will be totaled to determine the team's overall score.",
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.skillsusa.org/',
              title: 'Home Page'
            }
          ],
          participation: 11,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      'SoCal Cyber Cup': {
        aliases: ['SoCal Cyber Cup'],
        name: 'SoCal Cyber Cup',
        properties: {
          description:
            'Join the SoCal Cyber Cup Challenge and learn about cybersecurity careers and defend against security vulnerabilities in the California counties of Imperial, Orange, Riverside, San Bernardino, and San Diego. The Challenge will immerse you to solve a variety of cybersecurity challenges that range from incident response to web application security and will culminate in a live attack from the red team.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://cyberskyline.com/events/socalccc',
              title: 'Home Page'
            }
          ],
          participation: 2,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      'Space Heroes CTF': {
        aliases: ['Space Heroes CTF'],
        name: 'Space Heroes CTF',
        properties: {
          description:
            'Space Heroes CTF is an online jeopardy style Capture the Flag competition hosted by the FITSEC Team From Florida Tech.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://ctftime.org/ctf/728/',
              title: 'CTFTime'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF'],
          teams: null,
          virtual: null
        }
      },
      SpartaHacks: {
        aliases: ['SpartaHacks'],
        name: 'SpartaHacks',
        properties: {
          description:
            "SpartaHack is designed to deliver a great experience for participants of all majors & skill levels.. We'll let the numbers speak for themselves.",
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://spartahack.com/',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      'Splunk Boss of the SOC': {
        aliases: ['Splunk Boss of the SOC competition', 'splunk boss of the soc'],
        name: 'Splunk Boss of the SOC',
        properties: {
          description:
            'Boss of the SOC v.9. June 11 at 7:30 pm. Doors open at 6:45 pm. Join us for the 9th annual Boss of the SOC (BOTS) competition during .conf24. In this blue-team, CTF (ish) event, competitors will play as Alice Bluebird, a security analyst protecting her organization, Frothly, against Angry Alpaca, a new APT group unveiled at .conf23.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://conf.splunk.com/connect.html',
              title: 'Home Page'
            }
          ],
          participation: 3,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF', 'Conference'],
          teams: null,
          virtual: null
        }
      },
      'Spokane Cyber Cup': {
        aliases: ['Spokane Cyber Cup', "spokane mayor's cup"],
        name: 'Spokane Cyber Cup',
        properties: {
          description:
            'Players can sign up as solo and will be placed with a group of other students as well. Saturday, February 10th, 2024. Eastern Washington University. Catalyst Building. 601 E. Riverside Ave. Spokane, WA 99202. Registered teams must check-in between 8:30 and 9 the morning of the event for room assignments. Check-in will be held in the Catalyst ...',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://spokanectf.github.io/',
              title: 'Home Page'
            }
          ],
          participation: 3,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      SunshineCTF: {
        aliases: ['SunshineCTF'],
        name: 'SunshineCTF',
        properties: {
          description:
            'SunshineCTF 2023. START: 2023-10-07 14:00 UTC. FINISH: 2023-10-09 14:00 UTC. CTF Type: Jeopardy. Categories: The usual suspects. Flag Format: sun{stuffs} Infra sponsored by goo.gle/ctfsponsorship. Register Now. Hack@UCF is hosting the 2023 SunshineCTF competition October 7-9 2023 at BSides Orlando.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://sunshinectf.org/',
              title: 'Home Page'
            }
          ],
          participation: 2,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF'],
          teams: null,
          virtual: null
        }
      },
      SwampCTF: {
        aliases: ['SwampCTF'],
        name: 'SwampCTF',
        properties: {
          description:
            "SwampCTF is the annual jeopardy-style CTF hosted by the University of Florida's Student InfoSec Team (aka: UFSIT, aka aka: Kernel Sanders). Our most successful competition yet, SwampCTF 2024, was collectively rated by competitors 40.97 out of 42! April 5th-7th 2024.",
          education_target: ['Early College', 'College'],
          eligibility: 'Open',
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://swampctf.com/',
              title: 'Home Page'
            },
            {
              category: 'website',
              description: null,
              link: 'https://swampctf.com/instructions.html',
              title: 'Rules and Instructions'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: true,
          tags: ['CTF'],
          teams: null,
          virtual: true
        }
      },
      'TAMU CTF': {
        aliases: ['TAMUctf'],
        name: 'TAMU CTF',
        properties: {
          description:
            "TAMUctf, a CTF competition 100% developed, designed, and hosted by fellow Aggies, will run from Friday, April 5th, 5:00 pm CDT to Sunday, April 7th, ... don't hesitate to get in touch with the development team at ctf@tamu.edu. 2024 TAMUctf Dates. START: Friday, April 5, 2024 at 5:00 PM CST. END: Sunday, April 7, 2024 at 5:00 PM CST.",
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://cybersecurity.tamu.edu/tamuctf/',
              title: 'Home Page'
            }
          ],
          participation: 2,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF'],
          teams: null,
          virtual: null
        }
      },
      'The Diana Initiative CTF': {
        aliases: ['The Diana Initiative CTF', 'TDI CTF', 'CTF at The Diana Initiative'],
        name: 'The Diana Initiative CTF',
        properties: {
          description:
            'Welcome to the Capture the Flag  (CTF) competition at The Diana Initiative (TDI)! We have lots to offer players of all backgrounds and levels. This year we are offering two different CTFs which will run during our one day event, August 5, 2024.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.dianainitiative.org/event-2024/capture-the-flag-competition-2024/',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF', 'Conference', 'Crypto', 'Reversing', 'Forensics', 'Exploitation'],
          teams: null,
          virtual: null
        }
      },
      TracerFire: {
        aliases: ['tracefire', 'tracerfire'],
        name: 'TracerFire',
        properties: {
          description:
            'Tracer FIRE is a program developed by Sandia and Los Alamos National Laboratories to educate and train Cyber-Security Incident Responders, Analysts, and University Students, in critical skill areas including: incident response, forensic investigation and analysis, file systems, memory layout and malware analysis.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://sites.google.com/view/tracerfire-sandia-x-unlv/home',
              title: 'Home Page'
            }
          ],
          participation: 6,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      'Triangle InfoSeCon': {
        aliases: ['triangleinfosecon', 'raleigh issa infosecon'],
        name: 'Triangle InfoSeCon',
        properties: {
          description:
            'Triangle InfoSeCon stands out as the foremost annual Information Security conference in the region, proudly serving as the flagship event for the Raleigh Chapter of the Information System Security Association. Our mission is clear: to impart crucial knowledge on Information Security to as many individuals as possible.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.triangleinfosecon.com/',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: ['Conference', 'CTF'],
          teams: null,
          virtual: null
        }
      },
      'UAlbany GDDC': {
        aliases: ['Great Dane Defense Competition'],
        name: 'UAlbany GDDC',
        properties: {
          description: 'The Great Dane Defense Competition is a red vs. blue competition.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://uacyber.org/gddc',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: ['Red vs Blue'],
          teams: null,
          virtual: null
        }
      },
      'UBNetDef Lockdown': {
        aliases: ['NetDef', 'University at Buffalo Lockdown', 'UB NetDef', 'UBNetDef LockdownV12', 'ub  lockdown'],
        name: 'UBNetDef Lockdown',
        properties: {
          description:
            'Lockdown is a defensive security competition run by the University at Buffalo Network Defense. Competitors are placed in a simulated environment where they will have to defend against attackers trying to bring down their network. Not only do the competitors have to keep their network up, they will have to complete tasks throughout the day that ...',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://lockdown.ubnetdef.org/',
              title: 'Home Page'
            }
          ],
          participation: 3,
          practice_available: null,
          qualifiers: null,
          tags: ['Red vs Blue', 'Defensive'],
          teams: null,
          virtual: null
        }
      },
      'US Cyber Challenge': {
        aliases: ['US Cyber Challenge', 'Cyber Quests', 'USCC'],
        name: 'US Cyber Challenge',
        properties: {
          description:
            'US Cyber Challenge (USCC) is a program that identifies, trains, and places the next generation of cybersecurity professionals into the workforce. It offers online competitions, cyber camps, and job fairs for participants to demonstrate their skills and connect with the industry.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.uscyberchallenge.org/',
              title: 'Home Page'
            }
          ],
          participation: 5,
          practice_available: null,
          qualifiers: null,
          tags: ['Forensics', 'CTF'],
          teams: null,
          virtual: null
        }
      },
      'US Cyber Open': {
        aliases: ['US Cyber Open'],
        name: 'US Cyber Open',
        properties: {
          description:
            "US Cyber Open May 31 - June 9, 2024 . Beginner's Game Room Friday, May 31st thru Sunday, June 9th. The Beginner's Game Room is an entry-level CTF, and a great place to solve your first challenge(s) or warm-up for the hefty competition in our Competitive CTF. Competitive CTF",
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.uscybergames.com/',
              title: 'Home Page'
            }
          ],
          participation: 2,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF'],
          teams: null,
          virtual: null
        }
      },
      'UTEP-CS & ARL Hackathon': {
        aliases: ['UTEP-CS-ARL Cybersecurity Hackathon'],
        name: 'UTEP-CS & ARL Hackathon',
        properties: {
          description:
            'Learn the inner workings of critical and devastating malware that is still inuse today! Make your mark\u2013help to develop novel solutions to stop this and similar malware.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.utep.edu/cs/news/arl south cybersecurity rig malware hackathon.html',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      'University of Massachusetts CTF': {
        aliases: ['University of Massachusetts CTF'],
        name: 'University of Massachusetts CTF',
        properties: {
          description:
            'Friday, 04/19/2024 6:00pm to Sunday, 04/21/2024 6:00pm. The third annual UMassCTF will be held from Apr. 19 at 6:00 p.m. to Apr. 21 at 6:00 p.m.. This jeopardy-style virtual capture the flag will feature student and company-made challenges with a wide variety of topics including web exploitation, reverse engineering, cryptography, forensics ...',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.cics.umass.edu/event/20240419/umassctf-0',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF'],
          teams: null,
          virtual: null
        }
      },
      'University of Purdue CTF': {
        aliases: ['University of Purdue CTF'],
        name: 'University of Purdue CTF',
        properties: {
          description:
            'b01lers CTF 2024. 00: 00: 00: 00. b01lers CTF is over! b01lers CTF is the public competitive CTF hosted by the b01lers CTF team at Purdue University. Join our discord at discord.gg/tBMqujE and look out for further info soon! Rules Prizes Sponsors.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://b01lersc.tf/',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF'],
          teams: null,
          virtual: null
        }
      },
      'University of Texas CTF': {
        aliases: ['University of Texas CTF'],
        name: 'University of Texas CTF',
        properties: {
          description:
            'Online, jeopardy-style CTF run by the Information & Systems Security Society at the University of Texas at Austin. Free to play, with prizes offered to University teams. Open to all! Problems will span difficulties ranging from beginner-friendly to extremely challenging. Run by the UTISS CTF Team, with members from the Information & Systems Security Society.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.isss.io/utctf/',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF'],
          teams: null,
          virtual: null
        }
      },
      'VIVID Competition': {
        aliases: ['VIVID Competition', 'Vivid Competition', 'ace vivid'],
        name: 'VIVID Competition',
        properties: {
          description:
            'Phase 1: A 4 day experience in a themed virtual event consisting of CTF, Red Teaming, Blue Teaming, and King of the Hill! Phase 2: Select teams will be invited to participate at the live event at the CAE Annual Colloquium.',
          education_target: ['College'],
          eligibility: 'CAE Institutions',
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.caecommunity.org/news/vivid-competition-application-spring-2024',
              title: '2024 Spring Application'
            }
          ],
          participation: 14,
          practice_available: true,
          qualifiers: null,
          tags: ['CTF', 'Jeopardy', 'Offensive', 'Defensive', 'King of the Hill'],
          teams: '5 members',
          virtual: null
        }
      },
      'WIU LeatherHack': {
        aliases: ['LeatherHack'],
        name: 'WIU LeatherHack',
        properties: {
          description: 'A cybersecurity competition put on by WIU Cybersecurity Enthusiast Club.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.wiu.edu/cybersecuritycenter/competition/index.php',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: null
        }
      },
      'WPI CTF': {
        aliases: ['WPICTF'],
        name: 'WPI CTF',
        properties: {
          description:
            "WPI CTF 2023 - CANCELLED. SEE DISCORD. Hosted by WPI Cyber Security Club root@wpictf~# WHEN Cancelled. See Discord for more info root@wpictf~# WHO Anyone and everyone! ... CTF's can feel intimidating, but that's precisely why you should go to them, so that you can go from knowing nothing to something. ...",
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://wpictf.xyz/',
              title: 'Home Page'
            }
          ],
          participation: 2,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF'],
          teams: null,
          virtual: null
        }
      },
      'WiCyS CTF': {
        aliases: ['WiCyS Internal Competions and games', 'WiCyS-CTF'],
        name: 'WiCyS CTF',
        properties: {
          description:
            'WiCyS 2025 is the go-to event for cybersecurity professionals, students, and organizations. The conference is the flagship event to recruit, retain and advance women in cybersecurity \u2014 all while creating a community of engagement, encouragement and support at a technical conference for women and allies. The WiCyS 2025 Annual Conference will be held April 2-5, 2025 in Dallas, TX.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.wicys.org/events/wicys-2025/',
              title: 'Home Page'
            }
          ],
          participation: 3,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF', 'Conference'],
          teams: null,
          virtual: null
        }
      },
      'Wicked 6 Cyber Games': {
        aliases: ['Wicked 6 Cyber Games', 'Wicked 6', 'Wicked6'],
        name: 'Wicked 6 Cyber Games',
        properties: {
          description:
            "A 24-Hour Virtual Global Hack & Chat Event + An All Women's Tournament. Capture the Flag Challenges, Attack and Defend, Plus prizes & merch! March 28-29, 2025",
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.wicked6.com/en/',
              title: 'Home Page'
            }
          ],
          participation: 2,
          practice_available: null,
          qualifiers: null,
          tags: [],
          teams: null,
          virtual: true
        }
      },
      WolvCTF: {
        aliases: ['WolvCTF', 'University of Michigan CTF'],
        name: 'WolvCTF',
        properties: {
          description:
            'Our challenges range from beginner to hard in difficulty. Our esteemed members have worked tirelessly to bring new and stimulating CTF challenges to pose varying levels of difficulty and topic (RE, binary exploit, web exploit, cryptography, forensics, OSINT, misc.). There are two divisions, Open and University of Michigan students. No team limit sizes.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://ctftime.org/ctf/766',
              title: 'CTFTime'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF'],
          teams: null,
          virtual: null
        }
      },
      'Women in Technology (WiT) + Optiv Hackathon': {
        aliases: ['Cybersecurity Women in Technology (WiT) Optiv Hackathon'],
        name: 'Women in Technology (WiT) + Optiv Hackathon',
        properties: {
          description:
            'The WIT Campus + Optiv Hackathon is coming up on March 3th, 2023! You and your team of competitive hackers will get a chance to win EPIC grand prizes and giveaways. You\u2019ll hear from expert STEAM panelists and mentorships opportunities, and more.',
          education_target: null,
          eligibility: 'WIT Campus Club members',
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://mywit.org/wit-campus-hackthon/',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF', 'Conference'],
          teams: null,
          virtual: null
        }
      },
      'b01lers CTF': {
        aliases: ['b01lers CTF', 'b01ler ctf'],
        name: 'b01lers CTF',
        properties: {
          description:
            "Home : b01lers CTF - b01lers CTF 2023. CTF starts on Friday, March 17th at 21:00 UTC. CTF ends on Sunday, March 19th at 21:00 UTC. PRIZES: The Top Three Purdue University Teams Will. Recieve Binary Ninja Licenses! (Max 4 per team) We thank Trail of Bits for being a sponsor for this year's CTF event. If you would like to learn more about Trail ...",
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://ctf.b01lers.com/home',
              title: 'Home Page'
            }
          ],
          participation: 2,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF'],
          teams: null,
          virtual: null
        }
      },
      eCTF: {
        aliases: ['eCTF-MITRE', 'eCTF', 'Embedded Capture The Flag'],
        name: 'eCTF',
        properties: {
          description:
            'The eCTF is unique from other CTF competitions that you may be familiar with. First the focus of the eCTF is on embedded systems, which present a new set of challenges and security implications. Second, the eCTF balances defense and offense by testing and rewarding both sets of skills during the Design Phase and Attack Phase, respectively. Finally, to allow the complexity of the eCTF, the competition runs over the entire spring semester from mid-January through mid-April.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://ectfmitre.gitlab.io/ectf-website/index.html',
              title: 'Home Page'
            }
          ],
          participation: 19,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF', 'Embedded', 'Defensive', 'Offensive'],
          teams: null,
          virtual: null
        }
      },
      eCitadel: {
        aliases: ['eCitadel', 'CyberDiscord'],
        name: 'eCitadel',
        properties: {
          description:
            'The eCitadel Open (formerly CyberDiscord) is a free-to-play cyber defense competition that allows anyone to participate. Teams compete by securing provided Windows and Linux based virtual machines, maintaining critical services, resolving injects, and solving cybersecurity-related challenges. Teams accumulate points for addressing each scored issue and must race against the clock to accumulate as many points as they can before time expires. The 2024 eCitadel Open will be held May 17-19, 2024 and is free to enter.',
          education_target: null,
          eligibility: 'Open',
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://ecitadel.org/',
              title: 'Home Page'
            },
            {
              category: 'website',
              description: null,
              link: 'https://ecitadel.org/rules',
              title: 'Rules'
            },
            {
              category: 'registration',
              description: null,
              link: 'https://ecitadel.org/registration',
              title: 'Registration'
            }
          ],
          participation: 0,
          practice_available: null,
          qualifiers: false,
          tags: ['Defensive', 'Forensics'],
          teams: '2 - 4 members',
          virtual: true
        }
      },
      idekCTF: {
        aliases: ['idekCTF'],
        name: 'idekCTF',
        properties: {
          description:
            'idekCTF is an information security competition organized by the idek team and is aimed towards students and CTF veterans alike. It will cover the standard Jeopardy-style CTF topics (binary exploitation, reverse engineering, cryptography, and web exploitation) as well as other, less standard categories.',
          education_target: null,
          eligibility: null,
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://ctf.idek.team/',
              title: 'Home Page'
            }
          ],
          participation: 1,
          practice_available: null,
          qualifiers: null,
          tags: ['CTF'],
          teams: null,
          virtual: null
        }
      }
    },
    participation: {
      '247ctf': 1,
      'A-ISAC Student Cyber Challenge': 4,
      'ADMI Cybersecurity CTF': 1,
      'AFCEA TechNet Augusta International Military and Collegiate CTF': 1,
      'Alamo ACE': 2,
      'Aviation Cyber Initiative Cyber Rodeo CTF': 1,
      'BE Smart Hackathon': 2,
      'BlueHens CTF': 2,
      BuckeyeCTF: 3,
      'CNY Hackathon': 3,
      CSAW: 12,
      CUhackit: 1,
      'Collegiate Cyber Defense Competition': 140,
      'Colorado Cyber Games': 1,
      'Commonwealth Cyber Fusion': 14,
      'Conquer the Hill: Adventure Edition': 2,
      'Conquer the Hill: Reign Edition': 4,
      'Cyber 9/12': 4,
      'Cyber FastTrack': 6,
      'Cyber Panoply': 2,
      CyberBoat: 1,
      CyberQuest: 3,
      CyberSEED: 9,
      CyberTruck: 1,
      'CyberWarrior CTF': 1,
      Cybercup: 1,
      Cyberpatriot: 7,
      'Cybersecurity Entrepreneur Challenge': 1,
      Cyberstart: 4,
      Cyphercon: 2,
      DefCon: 7,
      DiceCTF: 3,
      'DigiKey Collegiate Computing Competition': 1,
      'DoD Cyber Sentinel Challenge': 1,
      'DoE CyberFire': 1,
      'DoE CyberForce': 76,
      DownUnderCTF: 4,
      'Global Collegiate Penetration Testing Competition (CPTC)': 45,
      'Google CTF': 3,
      GrizzHacks: 1,
      'HTB Cyber Apocalypse': 6,
      'HTB University CTF': 6,
      'Hack The Border': 1,
      'Hack The Box': 1,
      'Hack UNT': 1,
      'Hack Warz': 2,
      'Hack for Troops CTF': 1,
      'Hack this site': 1,
      'Hack-a-Sat': 3,
      'Hack@SCHack': 1,
      HackKean: 1,
      'HackPack CTF': 1,
      HackSMU: 1,
      'Hacky Holidays': 1,
      HeroCTF: 1,
      Hivestorm: 28,
      'ICL Collegiate Cup': 15,
      IEEEXtreme: 1,
      'ISACA North Texas Student Case Competition': 1,
      'ISEaGE Cyber Defense Competition': 1,
      ISTS: 8,
      'ISU CDC': 2,
      'IT-Olympics': 1,
      'Insidious Hackathon': 1,
      'International Collegiate Programming Contest': 4,
      JerseyCTF: 9,
      'Knight CTF': 1,
      KringleCon: 3,
      'LA CTF': 3,
      'Long Island CTF': 1,
      'MAGIC CTF': 1,
      'MGA CTF': 1,
      MHACKS: 1,
      'MISI Hack The Building: Hospital Edition': 11,
      'MISI Hack The Port': 38,
      'Maple CTF': 2,
      MetaCTF: 3,
      'Mountain West Cyber Challenge': 8,
      'NATO Locked Shields': 2,
      'NCAE Cyber Games': 113,
      NICCDC: 1,
      'NSA Codebreaker': 84,
      'NSA Cyber Exercise': 14,
      'NUARI TableTop': 2,
      'National Cyber League': 239,
      'NeverLAN CTF': 1,
      "NorCal Mayor's Cup": 2,
      'North Carolina Agricultural and Technical State University HACKNCAT': 1,
      'Palmetto Cyber Defense Contest National Cyber Exercise': 5,
      'Palo Alto Secure the Future': 2,
      PicoCTF: 16,
      "President's Cup": 3,
      PwnTillDawn: 1,
      'RM AFCEA CTF': 1,
      'RMCS CTF': 2,
      RUSecure: 6,
      'Raymond James CTF': 5,
      'RevolutionUC Hackathon': 1,
      'SAE CyberAuto': 1,
      'SANS Holiday Hack Challenge': 2,
      'SANS NetWars': 3,
      'SIGPwny UIUCTF': 1,
      'STEM CTF': 1,
      SkillsUSA: 11,
      'SoCal Cyber Cup': 2,
      'Space Heroes CTF': 1,
      SpartaHacks: 1,
      'Splunk Boss of the SOC': 3,
      'Spokane Cyber Cup': 3,
      SunshineCTF: 2,
      SwampCTF: 1,
      'TAMU CTF': 2,
      'The Diana Initiative CTF': 1,
      TracerFire: 6,
      'Triangle InfoSeCon': 1,
      TryHackMe: 3,
      'UAlbany GDDC': 1,
      'UBNetDef Lockdown': 3,
      'US Cyber Challenge': 5,
      'US Cyber Open': 2,
      'UTEP-CS & ARL Hackathon': 1,
      'University of Massachusetts CTF': 1,
      'University of Purdue CTF': 1,
      'University of Texas CTF': 1,
      'VIVID Competition': 14,
      'WIU LeatherHack': 1,
      'WPI CTF': 2,
      'WiCyS CTF': 3,
      'Wicked 6 Cyber Games': 2,
      WolvCTF: 1,
      'Women in Technology (WiT) + Optiv Hackathon': 1,
      'b01lers CTF': 2,
      eCTF: 19,
      idekCTF: 1
    },
    tags: [
      'King of the Hill',
      'Defensive',
      'Reversing',
      'OSINT',
      'Red vs Blue',
      'Recon',
      'Embedded',
      'Offensive',
      'Jeopardy',
      'Entrepreneur',
      'Crypto',
      'Networking',
      'Web',
      'Forensics',
      'Conference',
      'Exploitation',
      'CTF',
      'Programming'
    ],
    training: {
      '247ctf': {
        aliases: ['247ctf'],
        name: '247ctf',
        properties: {
          description:
            '247CTF is a security learning environment where hackers can test their abilities across a number of different Capture The Flag (CTF) challenge categories including web, cryptography, networking, reversing and exploitation.',
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://247ctf.com/',
              title: 'Home Page'
            }
          ],
          tags: ['CTF']
        }
      },
      'Hack The Box': {
        aliases: ['Hack-the-box'],
        name: 'Hack The Box',
        properties: {
          description:
            'Hack The Box offers a range of hacking experiences, from beginner to advanced, to help you improve your cybersecurity skills. Whether you are a hacker, a business or a university, you can join the platform and access gamified labs, challenges, courses and a community of learners.',
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://www.hackthebox.com/',
              title: 'Home Page'
            }
          ],
          tags: []
        }
      },
      'Mitre Cyber Academy': {
        aliases: ['Mitre Cyber Academy'],
        name: 'Mitre Cyber Academy',
        properties: {
          description:
            'Making security resources available to the people who need them. Challenge yourself in a variety of hands-on exercises.',
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://mitrecyberacademy.org/',
              title: 'Home Page'
            }
          ],
          tags: []
        }
      },
      TryHackMe: {
        aliases: ['TryHackMe'],
        name: 'TryHackMe',
        properties: {
          description:
            'Learn cyber security with fun interactive lessons and challenges on TryHackMe, a free online platform for all skill levels. Connect with other students, complete guided tasks and exercises, and join the community of 3 million registered users.',
          instances: [],
          links: [
            {
              category: 'website',
              description: null,
              link: 'https://tryhackme.com/',
              title: 'Home Page'
            }
          ],
          tags: []
        }
      }
    }
  }
};

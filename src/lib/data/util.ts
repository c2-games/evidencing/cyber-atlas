import type { Competition } from '@types';

export function SortByParticipation(comps: Competition[]) {
  return comps.sort((a, b) => b.properties.participation - a.properties.participation);
}

export function SortByName(comps: Competition[]) {
  return comps.sort((a, b) => a.name.localeCompare(b.name));
}

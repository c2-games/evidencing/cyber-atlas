export const Pathways = [
  {
    name: 'Education',
    description: 'Suggested path based on current education.',
    path: [
      {
        name: 'High School',
        competitions: ['PicoCTF', 'Cyberpatriot']
      },
      {
        name: 'Early College',
        competitions: ['NCAE Cyber Games', 'National Cyber League']
      },
      {
        name: 'Late College',
        competitions: [
          'Collegiate Cyber Defense Competition',
          'Global Collegiate Penetration Testing Competition (CPTC)',
          'DoE CyberForce'
        ]
      }
    ]
  },
  {
    name: 'Infrastructure Defense',
    description: 'Pathway for those interested in deploying, maintaining and defending secure infrastructure.',
    path: [
      {
        name: 'Beginner',
        competitions: ['NCAE Cyber Games']
      },
      {
        name: 'Intermediate',
        competitions: ['Hivestorm', 'VIVID Competition']
      },
      {
        name: 'Advanced',
        competitions: ['Collegiate Cyber Defense Competition', 'ISTS', 'DoE CyberForce']
      }
    ]
  },
  {
    name: 'Offensive Security',
    description:
      'Pathway for those interested in proactively securing infrastructure using tactics leveraged by malicious actors.',
    path: [
      {
        name: 'Beginner',
        competitions: ['National Cyber League']
      },
      {
        name: 'Intermediate',
        competitions: ['HTB University CTF']
      },
      {
        name: 'Advanced',
        competitions: ['Global Collegiate Penetration Testing Competition (CPTC)', 'eCTF', 'NSA Codebreaker']
      }
    ]
  }
];

import type { Competition, Training } from '@types';
import rawData from './rawData';

type EngagementDataType = {
  data: {
    competitions: Record<string, Competition>;
    tags: string[];
    training: Record<string, Training>;
    participation: Record<string, number>;
  };
};

export const EngagementData: EngagementDataType = rawData;

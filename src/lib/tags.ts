import type { CSSColorString } from 'svelvet/dist/types';

export type Tag = {
  color: CSSColorString;
  description: string;
  name: string;
};

// Prioritized list of known tags - a lower index means a higher priority.
export const knownTags: Tag[] = [
  {
    name: 'Red vs Blue',
    description: 'Multiple teams compete to attack and defend a system.',
    color: 'mediumpurple'
  },
  {
    name: 'Offensive',
    color: 'indianred',
    description: 'Participants are engaged in offensive cybersecurity operations'
  },
  {
    name: 'Defensive',
    color: 'dodgerblue',
    description: 'Participants are engaged in defensive cybersecurity operations'
  },
  {
    name: 'CTF',
    color: 'orange',
    description: 'Participants compete to find flags within specific challenges and environments'
  }
];

// returns a known tag object in prioritized order, if found.
export function findKnownTag(tags: string[] | undefined): Tag | null {
  if (!tags?.length) return null;

  for (const tag of knownTags) {
    // case-insensitive search
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/localeCompare
    if (tags.find((t2) => tag.name.localeCompare(t2, 'en', { sensitivity: 'base' }) === 0)) return tag;
  }

  return null;
}

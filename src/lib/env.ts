import * as ENVIRONMENT from '$env/static/public';

export type Environment = {
  UmamiTrackingId: string | null;
};

const Env: Environment = {
  UmamiTrackingId: ENVIRONMENT.PUBLIC_UMAMI_TRACKING_ID || null
};

console.debug('Env', Env);
export default Env;

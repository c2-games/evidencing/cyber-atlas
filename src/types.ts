export type Instance = {
  name: string;
  date: Date;
};

export type Link = {
  link: string;
  title: string;
  description?: string | null;
  category: string;
};

export type Properties = {
  description?: string | null;
  links: Link[];
  instances: Instance[];
  tags: string[];
};

export type CompetitionProperties = Properties & {
  education_target?: string[] | null;
  practice_available?: boolean | null;
  eligibility?: string | null;
  qualifiers?: boolean | null;
  teams?: string | null;
  virtual?: boolean | null;
  participation: number;
};

export type Competition = {
  name: string;
  aliases: string[];
  properties: CompetitionProperties;
};

export type Training = {
  name: string;
  aliases: string[];
  properties: Properties;
};

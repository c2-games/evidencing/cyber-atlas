type PageParameters = {
  params: { competitionId: string };
};

export async function load({ params }: PageParameters) {
  return {
    competitionId: params.competitionId
  };
}

// See https://kit.svelte.dev/docs/types#app
// for information about these interfaces
declare global {
  namespace App {
    // interface Error {}
    // interface Locals {}
    // interface PageData {}
    // interface Platform {}
    interface PageState {
      activeNode: string | null | undefined;
    }
  }

  interface Window {
    umami?: {
      track: (event: string, details?: null | Record<string, Any>) => void;
    };
  }
}

export {};
